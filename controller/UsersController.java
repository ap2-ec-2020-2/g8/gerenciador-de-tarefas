package controller;
import model.entity.Users;
import model.UsersDao;
import model.Database;

public class UsersController
{
    private UsersDao dao;
    
    public UsersController (Database database)
    {
        this.dao = new UsersDao(database);
    }
    
    public void create (Users username) throws Exception
    {
        dao.create(username);
    }
    
     public Users getUsernames(int id) throws Exception
    {
        return dao.getUsernames(id);
    }
    
    public void update (Users username) throws Exception 
    {
        dao.update(username);
    }
    
    public int loadUsers(String username, String senha) throws Exception
    {
         return dao.loadUsers(username, senha);
    }
    
    public void existUsers(String username) throws Exception
    {
        dao.existUsers(username);
    }
    public int qntRegistros()  throws Exception
    {
        try
        {
            return dao.registros();
        }
        catch(Exception e)
        {
            throw new Exception();
        }
    }
}
