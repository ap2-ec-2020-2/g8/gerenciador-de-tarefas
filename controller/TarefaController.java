package controller;
import java.util.List;
import java.util.ArrayList;

import model.entity.Tarefa;
import model.TarefaDao;
import model.Database;

public class TarefaController
{
    private TarefaDao dao;
    
    public TarefaController (Database database)
    {
        this.dao = new TarefaDao(database);
    }
    
    public void create (Tarefa tarefa) throws Exception
    {
        dao.create(tarefa);
    }
    
    public List<Tarefa> readAll() throws Exception
    {
        return dao.readAll();
    }
    
    public void state(Tarefa tarefa, boolean estado) throws Exception
    {
        dao.state(tarefa,estado);
    }
    
    public Tarefa loadFromId(int id) throws Exception
    {
        try
        {   
            Tarefa tarefa;
            tarefa = dao.loadFromId(id);
            return tarefa;
        }
        catch(Exception e)
        {
            throw new Exception("N�o h� tarefa com este Id.");
        }
    }
    
    public void update (Tarefa tarefa)throws Exception
    {
        dao.update(tarefa);
    }
    
    public void delete(Tarefa tarefa) throws Exception
    {
        dao.delete(tarefa);
    }
}
