package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Tarefa;

public class TarefaDao
{
    public static Database database;
    public static Dao<Tarefa, Integer> dao;
    private Tarefa tarefaCarregada;
    private List<Tarefa> tarefasCarregadas;
    
    public TarefaDao (Database database)
    {
        TarefaDao.setDatabase(database);
        tarefasCarregadas = new ArrayList<Tarefa>();
    }
    
    public static void setDatabase(Database database)
    {
        TarefaDao.database = database;
        try
        {
            dao = DaoManager.createDao(database.getConnection(), Tarefa.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Tarefa.class);
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }
    
    public void create (Tarefa tarefa) throws Exception
    {
       int nrows = 0;
       try
       {
           nrows = dao.create(tarefa);
           if(nrows == 0)
                throw new SQLException("Erro ao criar a tarefa");
           this.tarefaCarregada = tarefa;
           tarefasCarregadas.add (tarefa);
       }
       catch(SQLException e)
       {
           throw new Exception();
       }
    }   
    
    public List<Tarefa> readAll() throws Exception
    {
        try 
        {
            this.tarefasCarregadas = dao.queryForAll();
            if(this.tarefasCarregadas.size() !=0)
                this.tarefaCarregada = this.tarefasCarregadas.get(0);
        }
            catch(SQLException e)
        {
           throw new Exception();
        }
        
        return this.tarefasCarregadas;
    }
    
   
    
    
    public Tarefa loadFromId(int id) throws Exception
    {
        try {
            this.tarefaCarregada = dao.queryForId(id);
            if (this.tarefaCarregada != null)
                this.tarefasCarregadas.add(this.tarefaCarregada);
        } catch (SQLException e)
        {
            throw new Exception("Tarefa n�o encontrada");
        }
        return this.tarefaCarregada;
    }    
    
    
    public void update (Tarefa tarefa) throws Exception
    {
        try
        {
            dao.update(tarefa);
        }
        catch (java.sql.SQLException e)
        {
            throw new Exception();
        }   
    }
    
    public void state(Tarefa tarefa, boolean estado) throws Exception
    {
        tarefa.setComplet(estado);
        try
        {
            dao.update(tarefa);
        }
        catch (java.sql.SQLException e)
        {
            throw new Exception();
        }
    }
    
    public void delete (Tarefa tarefa) throws Exception
    {
        try 
        {
            dao.deleteById(tarefa.getId());
        }
        catch (SQLException e)
        {
           throw new Exception(); 
        }
    }          
}
