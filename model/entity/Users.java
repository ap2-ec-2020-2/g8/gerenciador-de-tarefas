package model.entity;

import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import java.sql.SQLException;
import java.time.ZoneId;
/**
 * Escreva a descri��o da classe Username aqui.
 * 
 * @author (seu nome) 
 * @version (n�mero de vers�o ou data)
 */
public class Users
{
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField
    private String username;
    
    @DatabaseField
    private String senha;

    

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie username*/
    public String getUsername(){
        return this.username;
    }//end method getUsername

    /**SET Method Propertie username*/
    public void setUsername(String username){
        this.username = username;
    }//end method setUsername

    /**GET Method Propertie senha*/
    public String getSenha(){
        return this.senha;
    }//end method getSenha

    /**SET Method Propertie senha*/
    public void setSenha(String senha){
        this.senha = senha;
    }//end method setSenha

    //End GetterSetterExtension Source Code
//!
}
