package model.entity;
import java.util.Date;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.time.ZoneId;
@DatabaseTable(tableName = "Tarefas")
public class Tarefa
{   
    
    @DatabaseField(generatedId = true)
    private int id;
    
    @DatabaseField
    private String name;
    
    @DatabaseField
    private String descri;
    
    @DatabaseField(dataType=DataType.DATE)
    public Date registrationDate;
    
    @DatabaseField(dataType=DataType.DATE)
    public Date endDate;    
    
    @DatabaseField
    private boolean complet;
    
    public String printEndDate() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(endDate.getTime());
    }
    
    public String printRegistrationDate() {
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        return dateFor.format(registrationDate);
    }
    //Start GetterSetterExtension Source Code
    
    public String getState()
    {
        if(complet) return "Conclu�da";
        else return "-           ";
    }
    
    /**GET Method Propertie id*/
    public int getId(){
        return this.id;
    }//end method getId

    /**SET Method Propertie id*/
    public void setId(int id){
        this.id = id;
    }//end method setId

    /**GET Method Propertie name*/
    public String getName(){
        return this.name;
    }//end method getName

    /**SET Method Propertie name*/
    public void setName(String name){
        this.name = name;
    }//end method setName

    /**GET Method Propertie registrationDate*/
    public Date getRegistrationDate(){
        return this.registrationDate;
    }//end method getRegistrationDate

    /**SET Method Propertie registrationDate*/
    public void setRegistrationDate(Date date){
        this.registrationDate = date;
    }//end method setRegistrationDate

    /**GET Method Propertie endDate*/
    public Date getEndDate(){
        return this.endDate;
    }//end method getEndDate

    /**SET Method Propertie endDate*/
    public void setEndDate(Date endDate){
        this.endDate = endDate;
    }//end method setEndDate

    /**GET Method Propertie complet*/
    public boolean getComplet(){
        return this.complet;
    }//end method getComplet

    /**SET Method Propertie complet*/
    public void setComplet(boolean complet){
        this.complet = complet;
    }//end method setComplet

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie descri*/
    public String getDescri(){
        return this.descri;
    }//end method getDescri

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**SET Method Propertie descri*/
    public void setDescri(String descri){
        this.descri = descri;
    }//end method setDescri

    //End GetterSetterExtension Source Code
//!
}