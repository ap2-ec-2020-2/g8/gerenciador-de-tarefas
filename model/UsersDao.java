package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.ArrayList;
import java.util.List;
import model.entity.Users;

public class UsersDao
{
    public static Database database;
    public static Dao<Users, Integer> dao;
    private List<Users> listaUserss;
    public UsersDao (Database database)
    {
        UsersDao.setDatabase(database);
        listaUserss = new ArrayList<Users>();
    }

    public static void setDatabase(Database database)
    {
        UsersDao.database = database;
        try
        {
            dao = DaoManager.createDao(database.getConnection(), Users.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Users.class);
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }

    public void create (Users user) throws Exception
    {
        int nrows = 0;
        try
        {
            nrows = dao.create(user);
            if(nrows == 0)
                throw new SQLException("Erro ao criar o user");
        }
        catch(SQLException e)
        {
            throw new Exception();
        }
    }   

    public Users getUsernames(int id)  throws Exception
    {
        Users user = dao.queryForId(id);
        return user;
    }
    
    
    public int loadUsers(String user, String senha) throws Exception
    {
        try 
        {
            listaUserss = dao.queryForAll(); 
            int id = -1;
            int login = 0;
            for(int i = 0; i < listaUserss.size(); i++)
            {
                if(user.equals(listaUserss.get(i).getUsername()) && senha.equals(listaUserss.get(i).getSenha()))
                {
                    id = listaUserss.get(i).getId();
                    login = 1;
                }
            }
            if(login == 0) throw new Exception();
            return id;
        } 
        catch (Exception e)
        {
            throw new Exception("Usu�rio e/ou senha incorreto(s). \n\tTente novamente ");
        }
    }  

    public void existUsers(String user) throws Exception
    {
        try 
        {
            listaUserss = dao.queryForAll(); 
            int encontrado =0;
            if(listaUserss.size()>0)
            {
                for(int i = 0; i < listaUserss.size(); i++)
                {
                    if(user.equals(listaUserss.get(i).getUsername()))
                    {
                        i = listaUserss.size();
                        encontrado = 1;
                    }
                }
                if(encontrado == 1) throw new Exception();
            }
        } 
        catch (SQLException e)
        {
            throw new Exception();
        }
    }

    public void update (Users user)
    {
        try
        {
            dao.update(user);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }   
    }

    public int registros() throws Exception
    {
        try
        {
            listaUserss = dao.queryForAll(); 
            return listaUserss.size();
        }catch(Exception e)
        {
            throw new Exception();
        }
    }
}
