![enter image description here](https://lh3.googleusercontent.com/pw/ACtC-3fylcJlcPoERRDHvSuOv3Vco6p-WKnTJGj_hTV3M8u179r9M-jg9kBTixRQlEKjx8aRI1c2UF89qSH-qG6g922jJ5KWdBSV0cb4rM7UjFAEwsdeLAuVkegw5d0Vm0OmOhLbP9C3jvG-XLSvGRMa_BA=s500-no?authuser=0)
# Sejam bem-vindos ao EasyTask

Esse projeto foi desenvolvido por estudantes da Universidade Federal de Goiás para a disciplina de **Algoritmos e Programação 2**, ministrada pelos professores **[MARCELO AKIRA INUZUKA](http://www.docente.ufg.br/marceloakira "Acesse a página pública deste docente")** e **[WILLIAM DIVINO FERREIRA](http://www.docente.ufg.br/wferreira7 "Acesse a página pública deste docente")** .

Após muita pesquisa e levantamento de informações os estudantes Eduardo Caixeta @EduardoCaixeta (Líder), Guilherme Rodrigues @guilhermerogom, Lucas Ferreira de Lima @lima.de e Victor Hugo Magalhães @VictorMagal perceberam que existe uma coisa em comum à maioria dos estudantes de Engenharia: falta de planejamento.

Estudos, provas, reuniões e trabalhos passaram a ser um fardo a ser carregado pelos estudantes que decidiram procurar um meio para organizar toda sua rotina. Após isso, veio a ideia de um gerenciador de tarefas voltado ao estudante de Engenharia.


# Funcionamento

Antes da utilização do App, o novo usuário deverá realizar um cadastro composto por "Usuário'', "Senha" e "Confirme"; rápido e prático.

Após o cadastro e o posterior Login, o usuário terá acesso a um painel simplificado com opções de "Início", "Tarefas", "Cadastro", "Configurações" e "Help". O botão sobre informa ao usuário o propósito do programa, mas não possui utilidade que não seja informação.

Em tarefas o usuário poderá adicionar obrigações a serem cumpridas e registra-las com nome, descrição e data final de entrega; após o cadastro, um número de ID será gerado para aquela tarefa que ficará salvo no usuário cadastrado.




## Classes implementadas

- UsuarioDao
- Database
- TarefaDao
 - Usuario	
- Tarefa
- UsuarioController
- TarefaController
- Config
- Framelogin
- ListarTarefas
- Frame
- App
- FrameTarefa
- Limite_digitos
- StringTarefa
- PanelCadastro
- PanelLogin
- Panel
- Panellinicio
- PanelSobre
- Fundos



## Resumo


```mermaid
graph LR
A[Usuário] -- Obrigações --> B((Tarefas))
A --> C(Adicionar/Modificar/Criar)
B --> C
C --> B
B --> D{Obrigação Concluída}
```

A --> C(Adicionar/Modificar/Criar)
B --> C
B --> D{Obrigação Concluída}
C --> D
