
package view;

import javax.swing.*;
import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.Database;
import model.entity.Tarefa;
import java.util.List;
import java.util.ArrayList;
import controller.TarefaController;
import view.panels.Fundos;
import view.panels.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.format.StringTarefa;
public class ListarTarefas implements ActionListener
{
    private JTextArea txtArea;

    private Fundos fundo = new Fundos();
    private JPanel panel = new JPanel();

    private JPanel scrol = new Panel(fundo.getLista());
    private int vazia;
    private String appName;
    private Database database;
    private List<Tarefa> listTarefas;
    private ArrayList<JCheckBox> check;
    private Tarefa tarefa;
    private TarefaController controller;
    private JFrame frame;
    private JLabel labelPages;
    private JButton buttonMarcaAll;
    private JButton buttonConcluir;
    private JButton buttonDesconcluir;
    private JButton buttonDelete;
    private JButton buttonExit;
    private JButton buttonPreviousPage;
    private JButton buttonNextPage;
    private ArrayList<JLabel> checkDate;
    private ArrayList<JLabel> labelName;
    private SpringLayout layout = new SpringLayout();
    private ArrayList<JButton> buttons = new ArrayList<JButton>();
    private Utils util = new Utils();
    private int page;
    private int mark;
    private int listLength;
    private int qntPages;
    public ListarTarefas (Database database, String nome, JFrame frame)
    {
        this.appName = nome;
        this.page = 1;
        this.database = database;

        this.frame = frame;
        this.controller = new TarefaController(database);
    } 

    public void adicionarButtons()
    {
        buttons.clear();
        scrol.removeAll();
        buttonMarcaAll = new JButton("Marcar/Desmarcar Todos");

        buttons.add(buttonMarcaAll);
        buttonMarcaAll.addActionListener(this);

        buttonConcluir = new JButton("Concluir");
        buttons.add(buttonConcluir);
        buttonConcluir.addActionListener(this);

        buttonDesconcluir = new JButton("N�o conclu�da");
        buttons.add(buttonDesconcluir);
        buttonDesconcluir.addActionListener(this);

        buttonDelete = new JButton("EXCUIR");
        buttonDelete.addActionListener(this);
        buttons.add(buttonDelete);

        buttonExit = new JButton("Sair");
        buttonExit.setBorderPainted(false);
        buttonExit.setBackground(Color.RED);
        scrol.add(buttonExit);
        buttonExit.addActionListener(this);

        buttonNextPage  = new JButton(fundo.getNext());

        scrol.add(buttonNextPage);

        if(page >= qntPages) 
        {
            buttonNextPage.setEnabled(false);

        }
        buttonNextPage.addActionListener(this);

        buttonPreviousPage = new JButton(fundo.getPrevious());

        scrol.add(buttonPreviousPage);
        if(page <= 1) 
        {
            buttonPreviousPage.setEnabled(false);
            if(page == 0)
            {
                buttonMarcaAll.setEnabled(false);
                buttonConcluir.setEnabled(false);
                buttonDesconcluir.setEnabled(false);
                buttonDelete.setEnabled(false);
            }

        }
        buttonPreviousPage.addActionListener(this);

        labelPages = new JLabel(String.valueOf(page) +" / "+String.valueOf(qntPages));
        labelPages.setForeground(Color.WHITE);
        labelPages.setSize(30, 15);
        scrol.add(labelPages);
        layout.putConstraint(SpringLayout.WEST, labelPages ,
            243,
            SpringLayout.WEST, scrol);
        layout.putConstraint(SpringLayout.NORTH, labelPages,
            25*(7-1)+98,
            SpringLayout.NORTH, scrol);

        int w = buttons.size();
        scrol.add(buttons.get(0));
        layout.putConstraint(SpringLayout.WEST, buttons.get(0),
            20,
            SpringLayout.WEST, scrol);
        layout.putConstraint(SpringLayout.NORTH, buttons.get(0),
            5,
            SpringLayout.NORTH, scrol);

        buttons.get(0).setOpaque(false);
        buttons.get(0).setBorderPainted(false);
        buttons.get(0).setContentAreaFilled(false);
        buttons.get(0).setForeground(Color.GREEN);   
         buttons.get(0).setFont(new Font("Dialog", Font.BOLD, 13));

        for(int i = 1; i<w; i++)
        {
            scrol.add(buttons.get(i));
            buttons.get(i).setOpaque(false);
            buttons.get(i).setBorderPainted(false);
            buttons.get(i).setContentAreaFilled(false);
            if(i<w-1)buttons.get(i).setForeground(new Color(0, 255, 255));
            else buttons.get(i).setForeground(new Color(255 ,231 ,186));
            buttons.get(i).setFont(new Font("Dialog", Font.BOLD, 13));
            layout.putConstraint(SpringLayout.WEST, buttons.get(i),
                5,
                SpringLayout.EAST, buttons.get(i-1));
            layout.putConstraint(SpringLayout.NORTH, buttons.get(i),
                5,
                SpringLayout.NORTH, scrol);
        }

        buttonNextPage.setOpaque(false);
        buttonNextPage.setBorderPainted(false);
        buttonNextPage.setContentAreaFilled(false);
        buttonPreviousPage.setOpaque(false);
        buttonPreviousPage.setBorderPainted(false);
        buttonPreviousPage.setContentAreaFilled(false);

        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);
        buttonExit.setForeground(Color.RED);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
            0 ,
            SpringLayout.EAST, scrol);     
        layout.putConstraint(SpringLayout.SOUTH, buttonExit,
            0,
            SpringLayout.SOUTH,scrol);

        layout.putConstraint(SpringLayout.EAST, buttonPreviousPage,
            252 ,
            SpringLayout.WEST, scrol);     
        layout.putConstraint(SpringLayout.NORTH, buttonPreviousPage,
            25*(6-1)+80 ,
            SpringLayout.NORTH,scrol);

        layout.putConstraint(SpringLayout.WEST, buttonNextPage,
            254 ,
            SpringLayout.WEST, scrol);     
        layout.putConstraint(SpringLayout.NORTH, buttonNextPage,
            25*(6-1)+80 ,
            SpringLayout.NORTH,scrol);          
    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonMarcaAll))
        {
            buttonMarcaAll_Click();
        }

        if(e.getSource().equals(buttonConcluir))
        {
            buttonConcluir_Click(); 
        }

        if(e.getSource().equals(buttonDesconcluir))
        {
            buttonDesconcluir_Click();

        }

        if(e.getSource().equals(buttonNextPage))
        {
            buttonNextPage_Click();
        }

        if(e.getSource().equals(buttonPreviousPage))
        {
            buttonPreviousPage_Click();
        }

        if(e.getSource().equals(buttonDelete))
        {
            buttonDelete_Click();
        }

        if(e.getSource().equals(buttonExit))
        {
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {      
                database.close();
                System.exit(0);
            }
        }
    }

    public void buttonConcluir_Click()
    {
        mark = -1;

        for(int i = 8*page - 8; (i < listLength && i < 8*page && mark != 1); i++)
        {
            if(check.get(i).isSelected())
            {
                if(listTarefas.get(i).getComplet()!=true)
                {
                    try
                    {
                        controller.state(listTarefas.get(i), true);
                        if (mark == -1)mark = 0;
                    }
                    catch(Exception e)
                    {
                        JOptionPane.showMessageDialog(frame
                        ,"N�o foi poss�vel alterar o status","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());   
                    }
                }
            }
        }
        if(mark == 0)
        {
            JOptionPane.showMessageDialog(frame
            ,"Tarefa(s) Conclu�da(s)","Tudo certo!", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getOk());
            scrol.removeAll();
            frame.add(setPanelList(true));
            util.setFrame(frame);  

        }
        else
        {
            JOptionPane.showMessageDialog(frame
            ,"N�o h� tarefas selecionadas","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }
    }

    public void buttonDesconcluir_Click()
    {
        mark = -1;

        for(int i = 8*page - 8; (i < listLength && i < 8*page && mark != 1); i++)
        {
            if(check.get(i).isSelected())
            {
                if(listTarefas.get(i).getComplet())
                {
                    try
                    {
                        controller.state(listTarefas.get(i), false);
                        if (mark == -1)mark = 0;

                    }
                    catch(Exception e)
                    {
                        JOptionPane.showMessageDialog(frame
                        ,"Erro: N�o foi poss�vel ler. \nDetalhes:"+ e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                    }
                }

            }
        }
        if(mark == 0)
        {
            JOptionPane.showMessageDialog(frame
            ,"Tarefa(s) marcada(s) como N�O conclu�da(s)","Tudo certo!", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getOk());
            scrol.removeAll();
            frame.add(setPanelList(true));
            util.setFrame(frame);  
        }
        else
        {
            JOptionPane.showMessageDialog(frame
            ,"N�o h� tarefas selecionadas","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }
    }

    public void buttonDelete_Click()
    {
        mark = -1;
        int posicoes[] = new int[listLength];
        int k = 0;
        for(int i = 8*page - 8; (i < listLength && i < 8*page); i++)
        {
            if(check.get(i).isSelected())
            {
                posicoes[k]=i;
                k++;
                if (mark == -1)mark = 0;
            }
        }
        if(mark == -1)
        {
            JOptionPane.showMessageDialog(frame,
                "N�o h� tarefas selecionadas","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }
        else 
        {
            int option = JOptionPane.showConfirmDialog(frame,
                    "Dseja excluir a tarefa atual?",
                    appName,
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, fundo.getExcluir());

            if (option == JOptionPane.YES_OPTION)
            {           
                for(int j = 0; j<k; j++)
                {
                    try
                    {
                        controller.delete(listTarefas.get(posicoes[j]));
                    }
                    catch(Exception e)
                    {
                        JOptionPane.showMessageDialog(frame
                        ,"Erro: N�o foi poss�vel apagar. \nDetalhes:"+ e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                    }
                }
                scrol.removeAll();
                frame.add(setPanelList(true));
                util.setFrame(frame);
            }
        }
    }

    public void buttonPreviousPage_Click()
    {
        page--;

        scrol.removeAll();
        frame.add(setPanelList(false));
        util.setFrame(frame);
    }

    public void buttonNextPage_Click()
    {
        page++;

        scrol.removeAll();
        frame.add(setPanelList(false));
        util.setFrame(frame);
    }

    public void buttonMarcaAll_Click()
    {
        mark = -1;
        for(int i = 8*page - 8; (i < listLength && i < 8*page); i++)
        {
            if(check.get(i).isSelected() != true) 
            {
                if (mark == -1) mark = 0;
                check.get(i).setSelected(true);

            }
        }
        if(mark == -1)
        {   
            for( int i = 8*page - 8; (i < listLength && i < 8*page); i++)
            {
                check.get(i).setSelected(false);
            }
        }
    }

    private void readTarefas()
    {
        try
        {
            this.listTarefas = controller.readAll();
            if (listTarefas.isEmpty())
            {
                page = 0;
                qntPages = 0;
                vazia = -1;
            }
            else
            {   
                do
                {
                    qntPages++;
                }
                while(7*qntPages < listTarefas.size());
                listLength = listTarefas.size();
                vazia = 0;
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(frame
            ,"Erro: N�o foi poss�vel ler. \nDetalhes:"+ e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }
    }

    public void criarChecks()
    {
        check = new ArrayList<JCheckBox>();
        for( int i = 0; i<listLength; i++)
        {
            check.add(new JCheckBox(""));
            check.get(i).setOpaque(false);
        }   
    }

    public JPanel setPanelList(boolean consultar)
    {

        if (consultar)
        {
            qntPages = 0;
            readTarefas();
        }
        if(vazia == 0)
        {

            panel.removeAll();

            layoutPanel(consultar);
        }
        else
        {
            scrol.setLayout(layout);
            JLabel noTasks = new JLabel("N�o h� tarefas registradas");
            noTasks.setFont(new Font("Dialog", Font.BOLD,20));
            
            
            adicionarButtons();
            noTasks.setForeground(Color.WHITE);
            scrol.add(noTasks);
            
            layout.putConstraint(SpringLayout.WEST, noTasks,
            130 ,
            SpringLayout.WEST, scrol);     
            layout.putConstraint(SpringLayout.NORTH, noTasks,
            110 ,
            SpringLayout.NORTH,scrol);
        
        }
        
        return scrol;
    }

    public void lerDados()
    {
        StringTarefa tostring = new StringTarefa();
        labelName = tostring.tarefaIdName(listTarefas,frame);
        checkDate = tostring.tarefaCheckDate(listTarefas,frame);
    }

    public void layoutPanel(boolean consultar)
    {
        panel.setLayout(layout);
        if (consultar)
        {
            criarChecks();
            lerDados();
        }
        int j=1;
        JLabel nameTabel = new JLabel("          Id                                       Nome    "+
                "                             Status           Data RG        Data Final");
        nameTabel.setFont(new Font("SansSerif", Font.BOLD, 12));
        nameTabel.setForeground(Color.WHITE);
        panel.add(nameTabel);
        layout.putConstraint(SpringLayout.WEST, nameTabel,
            3,
            SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, nameTabel,
            3,
            SpringLayout.NORTH, panel);

        for (int i = 7*page - 7; (i < listLength && i < 7*page); i++)
        {
            panel.add(check.get(i));
            panel.add(labelName.get(i));
            labelName.get(i).setForeground(Color.WHITE);
            labelName.get(i).setFont(new Font("SansSerif", Font.PLAIN, 12));
            panel.add(checkDate.get(i));
            checkDate.get(i).setForeground(Color.WHITE);
            checkDate.get(i).setFont(new Font("SansSerif", Font.PLAIN, 12));
            layout.putConstraint(SpringLayout.WEST, check.get(i),
                3,
                SpringLayout.WEST, panel);
            layout.putConstraint(SpringLayout.NORTH, check.get(i),
                20*j,
                SpringLayout.NORTH, panel);

            layout.putConstraint(SpringLayout.EAST, checkDate.get(i),
                -3,
                SpringLayout.EAST, panel);
            layout.putConstraint(SpringLayout.NORTH, checkDate.get(i),
                20*j+2,
                SpringLayout.NORTH, panel);   

            layout.putConstraint(SpringLayout.WEST, labelName.get(i),
                2,
                SpringLayout.EAST, check.get(i));
            layout.putConstraint(SpringLayout.NORTH, labelName.get(i),
                20*j+2,
                SpringLayout.NORTH, panel);
            layout.putConstraint(SpringLayout.EAST, labelName.get(i),
                -10,
                SpringLayout.WEST, checkDate.get(i));

            j++;

        }
        panel.setOpaque(false);
        JViewport viewport = new JViewport();
        viewport.setView(panel);
        viewport.setOpaque(false);
        panel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        scrollPane.setOpaque(false);
        scrollPane.setViewport(viewport);

        scrollPane.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0,Color.WHITE));
        scrol.setLayout(layout);
        adicionarButtons();
        scrol.add(scrollPane);

        layout.putConstraint(SpringLayout.WEST, scrollPane,
            20,
            SpringLayout.WEST, scrol);
        layout.putConstraint(SpringLayout.NORTH, scrollPane,
            5,
            SpringLayout.SOUTH, buttons.get(0));
        layout.putConstraint(SpringLayout.EAST, scrollPane,
            -20,
            SpringLayout.EAST, scrol);

        layout.putConstraint(SpringLayout.SOUTH, scrollPane,
            200,
            SpringLayout.NORTH, scrol);

    }

}

