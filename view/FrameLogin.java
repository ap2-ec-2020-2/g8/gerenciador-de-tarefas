package view;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import model.Database;
import view.panels.PanelLogin;
import java.awt.Toolkit;
import java.awt.Dimension;
import view.panels.Fundos;
public class FrameLogin
{
    Database database;
    String appName;
    JFrame frame = new JFrame("Login");
    public FrameLogin (Database db, String n)
    {
        this.database = db;
        this.appName = n;
        this.frame = new JFrame(appName +"  -  Login");
    }
    
    public void login()
    {
        PanelLogin panelLogin = new PanelLogin(database, appName);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        centerForm(frame);
        Fundos fundo = new Fundos();
        frame.setIconImage(fundo.getIcon().getImage());
        frame.add(panelLogin.alterarPanel(frame));
        
        frame.pack();
        frame.setSize(323,210);
        frame.setResizable(false);
        frame.setVisible(true);
    }
    
    private void centerForm(JFrame tela) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        int w = tela.getSize().width;
        int h = tela.getSize().height;
        int x = (dim.width-w)/3;
        int y = (dim.height-h)/3;

        tela.setLocation(x,y);       
    }
}

