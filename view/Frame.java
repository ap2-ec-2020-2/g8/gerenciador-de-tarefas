
package view;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.Box;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import model.Database;
import model.entity.Users;
import view.panels.*;
import java.awt.Toolkit;
import java.awt.Point;
import view.configs.Config;
public class Frame extends JFrame implements ActionListener
{
    private JFrame frame;
    private String appName;
    Fundos fundo = new Fundos ();
    Utils util = new Utils();
    JMenuItem menuInicio;
    JMenuItem menuAnalise;
    JMenuItem menuLista;
    JMenuItem menuHelp;
    JMenuItem menuSpace;
    Point xy;
    JMenuItem menuConfig;
    PanelLogin panelLogin;
    Database database;
    int logado = -1;
    public Frame (Database db,String n, Point xy)
    {
        this.database = db; 
        this.appName = n;
        this.xy = xy;
        alterarLayout();  
    }

    public void alterarLayout()
    {
        JMenuBar barra = new JMenuBar();
        menuInicio = new JMenuItem("      IN�CIO");
        menuInicio.setBackground(Color.WHITE);
        menuAnalise = new JMenuItem("TAREFAS");
        menuAnalise.setBackground(Color.WHITE);
        menuLista = new JMenuItem("CADASTROS");
        menuLista.setBackground(Color.WHITE);
        menuConfig = new JMenuItem("         CONFIG.");
        menuConfig.setBackground(Color.WHITE);

        menuHelp = new JMenuItem("      HELP");
        menuHelp.setBackground(Color.WHITE);

        barra.add(menuInicio); 
        barra.add(menuAnalise);

        barra.add(menuLista);

        barra.add(menuConfig);

            
        barra.add(menuHelp);

        menuInicio.setEnabled(false);        
        menuInicio.addActionListener(this);        
        menuAnalise.addActionListener(this);
        menuLista.addActionListener(this);
        menuHelp.addActionListener(this);
        menuConfig.addActionListener(this);

        frame = new JFrame("EasyTask");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setIconImage(fundo.getIcon().getImage());
        frame.setJMenuBar(barra);

        frame.setLocation(xy);
        menuInicio_Click();

        util.setFrame(frame);
    }  

    public void liberarButtons()
    {
        menuInicio.setEnabled(true);        
        menuAnalise.setEnabled(true);
        menuLista.setEnabled(true);
        menuHelp.setEnabled(true);
        menuConfig.setEnabled(true);
    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(menuInicio))
        {               
            menuInicio_Click();
        }

        if(e.getSource().equals(menuAnalise))
        {        
            menuAnalise_Click();
        }        

        if(e.getSource().equals(menuLista))
        {        
            menuLista_Click();
        }        

        if(e.getSource().equals(menuConfig))
        {        
            menuConfig_Click();
        } 
        
        if(e.getSource().equals(menuHelp))
        {        
            menuHelp_Click();
        }    
        
    }

    public void menuInicio_Click()    
    {               
        liberarButtons();
        menuInicio.setEnabled(false);
        PanelInicio panelInicio = new PanelInicio();   
        JPanel panel = panelInicio.setPanel(frame, "EasyTask");
        frame.getContentPane().removeAll();
        frame.add(panel);
        util.setFrame(frame);

    }

    public void menuAnalise_Click()    
    {          
        liberarButtons();
        menuAnalise.setEnabled(false);
        FormTarefa formTarefa = new FormTarefa(database, "EasyTask", frame);             
        Panel panel = formTarefa.createAndShowForm();
        frame.getContentPane().removeAll();
        frame.add(panel);
        util.setFrame(frame);
    }

    public void menuLista_Click()    
    {               
        liberarButtons();
        menuLista.setEnabled(false);
        ListarTarefas panelList = new ListarTarefas(database,"EasyTask", frame);
        frame.getContentPane().removeAll();
        JPanel panel = panelList.setPanelList(true);
        frame.getContentPane().removeAll();
        frame.add(panel);
        util.setFrame(frame);
    }

    public void menuConfig_Click()    
    {    
       liberarButtons();
       menuConfig.setEnabled(false);
       Config config = new Config(frame,appName, database);
       JPanel panel = config.setPanel();
       frame.getContentPane().removeAll();
       frame.add(panel);
       util.setFrame(frame);
    }

    public void menuHelp_Click()
    {
        liberarButtons();
        menuHelp.setEnabled(false);
        PanelHelp panel = new PanelHelp();
        frame.getContentPane().removeAll();
        frame.add(panel.setPanel(frame));
        util.setFrame(frame);
        
    }
    
}