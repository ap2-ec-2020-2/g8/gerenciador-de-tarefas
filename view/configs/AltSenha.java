
package view.configs;

import controller.UsersController;
import model.Database;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.SpringLayout;
import javax.swing.BorderFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;
import view.panels.Fundos;
import view.panels.Panel;
import model.entity.Users;
import view.format.Limite_digitos;
public class AltSenha implements ActionListener
{
    private JButton buttonVoltar;
    private JButton buttonSalvar;
    private JLabel oldSenha;
    private JLabel newSenha;
    private JLabel confirmNewSenha;
    private JPasswordField txtOldSenha;
    private JPasswordField txtSenha;
    private JPasswordField txtConfirmSenha;
    private JDialog dialog;
    private JPanel altSenha;
    private JFrame frame;
    private String appName;
    private Users antigoUser;
    private int id;
    private Database database;
    private UsersController controller;
    private Fundos fundo;
    public AltSenha(JFrame frame, String n)
    {
        this.frame = frame;
        appName = n.substring(7, n.length());
        database = new Database("users.db");
        controller = new UsersController(database);
        id = Integer.parseInt(appName);

        fundo = new Fundos();
    }

    public void getSenha()
    {try
        {
            antigoUser = controller.getUsernames(id);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(frame
            ,"Erro: N�o foi poss�vel encontrar a senha");
        }
    }   

    public void setDialog()
    {
        getSenha();
        dialog = new JDialog(frame, "Alterar Senha", true);
        altSenha = new Panel(fundo.getAlt());
        SpringLayout layout = new SpringLayout();
        altSenha.setLayout(layout);

        oldSenha = new JLabel("Senha atual:");
        oldSenha.setFont( new Font("Dialog", Font.BOLD, 13));
        oldSenha.setForeground(Color.WHITE);
        altSenha.add(oldSenha);

        layout.putConstraint(SpringLayout.WEST, oldSenha,
            40,
            SpringLayout.WEST, altSenha);     
        layout.putConstraint(SpringLayout.NORTH, oldSenha,
            15,
            SpringLayout.NORTH,altSenha);

        txtOldSenha = new JPasswordField(10);
        txtOldSenha.setForeground(Color.WHITE);
        txtOldSenha.setOpaque(false);
        txtOldSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtOldSenha.setDocument( new Limite_digitos(11, "[[ ]]"));
        txtOldSenha.setFont( new Font("Dialog", Font.BOLD, 13));
        altSenha.add(txtOldSenha);
        layout.putConstraint(SpringLayout.WEST, txtOldSenha,
            5,
            SpringLayout.EAST, oldSenha);     
        layout.putConstraint(SpringLayout.NORTH,txtOldSenha,
            15,
            SpringLayout.NORTH,altSenha);

        newSenha = new JLabel("Novo senha:");
        newSenha.setFont( new Font("Dialog", Font.BOLD, 13));
        newSenha.setForeground(Color.WHITE);
        altSenha.add(newSenha);

        layout.putConstraint(SpringLayout.WEST, newSenha,
            40,
            SpringLayout.WEST, altSenha);     
        layout.putConstraint(SpringLayout.NORTH, newSenha,
            45,
            SpringLayout.NORTH,altSenha);

        confirmNewSenha = new JLabel("Confirme:");
        confirmNewSenha.setFont(new Font("Dialog", Font.BOLD, 13));
        confirmNewSenha.setForeground(Color.WHITE);
        altSenha.add(confirmNewSenha);

        layout.putConstraint(SpringLayout.EAST, confirmNewSenha,
            0,
            SpringLayout.EAST, newSenha);     
        layout.putConstraint(SpringLayout.NORTH, confirmNewSenha,
            75,
            SpringLayout.NORTH,altSenha);

        txtSenha = new JPasswordField(10);
        txtSenha.setForeground(Color.WHITE);
        txtSenha.setDocument( new Limite_digitos(11,""));
        txtSenha.setOpaque(false);
        txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtSenha.setFont( new Font("Dialog", Font.BOLD, 13));
        altSenha.add(txtSenha);
        layout.putConstraint(SpringLayout.WEST, txtSenha,
            5,
            SpringLayout.EAST, newSenha);     
        layout.putConstraint(SpringLayout.NORTH,txtSenha,
            45,
            SpringLayout.NORTH,altSenha);

        txtConfirmSenha = new JPasswordField(10);
        txtConfirmSenha.setForeground(Color.WHITE);
        txtConfirmSenha.setOpaque(false);
        txtConfirmSenha.setDocument( new Limite_digitos(11,""));
        txtConfirmSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtConfirmSenha.setFont( new Font("Dialog", Font.BOLD, 13));
        altSenha.add(txtConfirmSenha);
        layout.putConstraint(SpringLayout.WEST, txtConfirmSenha,
            5,
            SpringLayout.EAST, confirmNewSenha);     
        layout.putConstraint(SpringLayout.NORTH,txtConfirmSenha,
            75,
            SpringLayout.NORTH,altSenha);

        buttonSalvar = new JButton("Salvar");

        buttonSalvar.setOpaque(false);
        buttonSalvar.setForeground(Color.WHITE);
        buttonSalvar.setContentAreaFilled(false);
        buttonSalvar.setBorderPainted(false);
        buttonSalvar.setFont( new Font("Dialog", Font.BOLD, 13));
        altSenha.add(buttonSalvar);
        layout.putConstraint(SpringLayout.WEST, buttonSalvar,
            115,
            SpringLayout.WEST, altSenha);     
        layout.putConstraint(SpringLayout.NORTH,buttonSalvar,
            35,
            SpringLayout.NORTH,txtConfirmSenha);

        buttonVoltar = new JButton("Voltar");
        buttonVoltar.setOpaque(false);
        buttonVoltar.setForeground(Color.WHITE);
        buttonVoltar.setContentAreaFilled(false);
        buttonVoltar.setBorderPainted(false);
        buttonVoltar.setFont( new Font("Dialog", Font.BOLD, 13));
        altSenha.add(buttonVoltar);
        layout.putConstraint(SpringLayout.WEST, buttonVoltar,
            0 ,
            SpringLayout.WEST, altSenha);     
        layout.putConstraint(SpringLayout.SOUTH, buttonVoltar,
            0,
            SpringLayout.SOUTH,altSenha);  

        buttonSalvar.addActionListener(this);
        buttonVoltar.addActionListener(this);  
        dialog.add(altSenha);

        dialog.setLocation(frame.getX() + 115,frame.getY()+ 100);
        dialog.setUndecorated(true);
        dialog.pack();
        dialog.setSize(323,180);
        dialog.setVisible(true);
    }

    public boolean getNewSenha()
    {
        try
        {
            txtOldSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            txtConfirmSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            if(txtOldSenha.getText().trim().isEmpty() == false)
            {
                if(txtSenha.getText().trim().isEmpty()==false)
                {
                    if(txtSenha.getText().equals(txtConfirmSenha.getText()))
                    {
                        if(txtSenha.getText().length() > 5 && txtSenha.getText().length() < 11)

                        {
                            if(txtOldSenha.getText().equals(antigoUser.getSenha()))
                            {
                                if(txtSenha.getText().equals(antigoUser.getSenha())==false) return true;
                                else
                                {
                                    JOptionPane.showMessageDialog(frame
                                    ,"Sua nova senha n�o pode ser igual a atual.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                                    txtOldSenha.setText("");
                                    txtSenha.setText("");
                                    txtConfirmSenha.setText("");

                                    throw new Exception();
                                }
                            }
                            else
                            {

                                txtOldSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED)); 
                                JOptionPane.showMessageDialog(frame
                                ,"Senha atual incorreta!","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());

                                throw new Exception(); 
                            }
                        }

                        else
                        {                
                            txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                            txtConfirmSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                            txtSenha.setText("");
                            txtConfirmSenha.setText("");
                            JOptionPane.showMessageDialog(frame
                            ,"A senha deve ter de 6 a 10 caracteres.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());

                            throw new Exception();
                        }
                    }
                    else
                    {

                        txtConfirmSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                        JOptionPane.showMessageDialog(frame
                        ,"A nova senha n�o foi confirmada.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                        txtConfirmSenha.setText("");

                        throw new Exception();
                    }
                }

                else
                {
                    txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                    JOptionPane.showMessageDialog(frame
                    ,"O campo \"Nova senha\" n�o pode estar vazio.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());

                    throw new Exception();
                }
            }
            else
            {
                JOptionPane.showMessageDialog(frame
                ,"O campo \"Senha atual\" n�o pode estar vazio.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                txtOldSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                throw new Exception();
            }
        }

        catch(Exception e)
        {
            return false;
        }
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(buttonVoltar))
        {  
            dialog.setVisible(false);
            dialog.dispose();
        }
        if(e.getSource().equals(buttonSalvar))
        {
            buttonSalvar_Click();
        }
    }

    public void buttonSalvar_Click()
    {
        if(getNewSenha())
        {
            Users updateSenha = antigoUser;
            updateSenha.setSenha(txtSenha.getText());
            try
            {
                controller.update(updateSenha);
                JOptionPane.showMessageDialog(frame
                ,"Senha alterada com sucesso.","Tudo certo!", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getOk());
                dialog.setVisible(false);
                dialog.dispose();
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(frame
                ,"N�o foi poss�vel alterar a senha.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
            }
        }
    }
}
