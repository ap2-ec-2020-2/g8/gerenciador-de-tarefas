
package view.configs;

import controller.UsersController;
import model.Database;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SpringLayout;
import javax.swing.BorderFactory;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;
import java.awt.Point;
import view.panels.Fundos;
import view.panels.Panel;
import model.entity.Users;
import view.format.Limite_digitos;
public class AltUser implements ActionListener
{
    private JButton buttonVoltar;
    private JButton buttonSalvar;
    private JLabel oldUser;
    private JLabel newUser;
    private JLabel confirmNewUser;
    private JLabel nameOldUser;
    private JTextField txtUser;
    private JTextField txtConfirmUser;
    private JDialog dialog;
    private JPanel altUser;
    private JFrame frame;
    private String appName;
    private Users antigoUser;
    private int id;
    private Database database;
    private UsersController controller;
    private Fundos fundo;
    public AltUser(JFrame frame, String n)
    {
        this.frame = frame;
        appName = n.substring(7, n.length());
        database = new Database("users.db");
        controller = new UsersController(database);
        id = Integer.parseInt(appName);
        fundo = new Fundos();
    }

    public void getUsername()
    {try
        {
            antigoUser = controller.getUsernames(id);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(frame
            ,"Erro: N�o foi poss�vel encontrar o atual username","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }
    }   

    public void setDialog()
    {
        getUsername();
        dialog = new JDialog(frame, "Alterar Username", true);      
        altUser = new Panel(fundo.getAlt());
        SpringLayout layout = new SpringLayout();
        altUser.setLayout(layout);

        oldUser = new JLabel("Username atual:");
        oldUser.setFont( new Font("Dialog", Font.BOLD, 13));
        oldUser.setForeground(Color.WHITE);
        altUser.add(oldUser);

        layout.putConstraint(SpringLayout.WEST, oldUser,
            40,
            SpringLayout.WEST, altUser);     
        layout.putConstraint(SpringLayout.NORTH, oldUser,
            15,
            SpringLayout.NORTH,altUser);

        nameOldUser = new JLabel(antigoUser.getUsername());
        nameOldUser.setFont( new Font("Dialog", Font.BOLD, 13));
        nameOldUser.setForeground(Color.WHITE);
        altUser.add(nameOldUser);
        layout.putConstraint(SpringLayout.WEST, nameOldUser,
            5,
            SpringLayout.EAST, oldUser);     
        layout.putConstraint(SpringLayout.NORTH,nameOldUser,
            15,
            SpringLayout.NORTH,altUser);

        newUser = new JLabel("Novo username:");
        newUser.setFont( new Font("Dialog", Font.BOLD, 13));
        newUser.setForeground(Color.WHITE);
        
        altUser.add(newUser);

        layout.putConstraint(SpringLayout.WEST, newUser,
            40,
            SpringLayout.WEST, altUser);     
        layout.putConstraint(SpringLayout.NORTH, newUser,
            45,
            SpringLayout.NORTH,altUser);

        confirmNewUser = new JLabel("Confirme:");
        confirmNewUser.setFont(new Font("Dialog", Font.BOLD, 13));
        confirmNewUser.setForeground(Color.WHITE);
        altUser.add(confirmNewUser);

        layout.putConstraint(SpringLayout.EAST, confirmNewUser,
            0,
            SpringLayout.EAST, newUser);     
        layout.putConstraint(SpringLayout.NORTH, confirmNewUser,
            75,
            SpringLayout.NORTH,altUser);

        txtUser = new JTextField(10);
        txtUser.setForeground(Color.WHITE);
        txtUser.setOpaque(false);
        txtUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtUser.setFont( new Font("Dialog", Font.BOLD, 13));
        txtUser.setDocument( new Limite_digitos(15, "[^a-z|^A-Z|^0-9[.][ ]]"));
        altUser.add(txtUser);
        layout.putConstraint(SpringLayout.WEST, txtUser,
            5,
            SpringLayout.EAST, newUser);     
        layout.putConstraint(SpringLayout.NORTH,txtUser,
            45,
            SpringLayout.NORTH,altUser);

        txtConfirmUser = new JTextField(10);
        txtConfirmUser.setForeground(Color.WHITE);
        txtConfirmUser.setOpaque(false);
        txtConfirmUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtConfirmUser.setFont( new Font("Dialog", Font.BOLD, 13));
        txtConfirmUser.setDocument( new Limite_digitos(15, "[^a-z|^A-Z|^0-9[.][ ]]"));
        altUser.add(txtConfirmUser);
        layout.putConstraint(SpringLayout.WEST, txtConfirmUser,
            5,
            SpringLayout.EAST, confirmNewUser);     
        layout.putConstraint(SpringLayout.NORTH,txtConfirmUser,
            75,
            SpringLayout.NORTH,altUser);

        buttonSalvar = new JButton("Salvar");

        buttonSalvar.setOpaque(false);
        buttonSalvar.setForeground(Color.WHITE);
        buttonSalvar.setContentAreaFilled(false);
        buttonSalvar.setBorderPainted(false);
        buttonSalvar.setFont( new Font("Dialog", Font.BOLD, 13));
        altUser.add(buttonSalvar);
        layout.putConstraint(SpringLayout.WEST, buttonSalvar,
            115,
            SpringLayout.WEST, altUser);     
        layout.putConstraint(SpringLayout.NORTH,buttonSalvar,
            35,
            SpringLayout.NORTH,txtConfirmUser);

        buttonVoltar = new JButton("Voltar");
        buttonVoltar.setOpaque(false);
        buttonVoltar.setForeground(Color.WHITE);
        buttonVoltar.setContentAreaFilled(false);
        buttonVoltar.setBorderPainted(false);
        buttonVoltar.setFont( new Font("Dialog", Font.BOLD, 13));
        altUser.add(buttonVoltar);
        layout.putConstraint(SpringLayout.WEST, buttonVoltar,
            0 ,
            SpringLayout.WEST, altUser);     
        layout.putConstraint(SpringLayout.SOUTH, buttonVoltar,
            0,
            SpringLayout.SOUTH,altUser);  

        buttonSalvar.addActionListener(this);
        buttonVoltar.addActionListener(this);  
        dialog.add(altUser);

        dialog.setLocation(frame.getX() + 115,frame.getY()+ 100);
        dialog.setUndecorated(true);
        dialog.pack();
        dialog.setSize(323,180);
        dialog.setVisible(true);

    }

    public boolean getNewUsername()
    {
        try
        {
            txtUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            txtConfirmUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            if(txtUser.getText().trim().isEmpty()==false)
                if(txtUser.getText().equals(txtConfirmUser.getText()))
                {
                    try
                    {
                        controller.existUsers(txtUser.getText());
                        return true;
                    }catch(Exception e)
                    {

                        txtUser.setText("");
                        txtUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                        txtConfirmUser.setText("");
                        txtConfirmUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                        JOptionPane.showMessageDialog(frame
                        ,"Nome de usu�rio j� cadastrado.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                        throw new Exception();
                    }
                }
                else
                {                
                    JOptionPane.showMessageDialog(frame
                    ,"O novo username n�o foi confirmado.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                    txtConfirmUser.setText("");
                    txtConfirmUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                    throw new Exception();
                }
            else
            {
                JOptionPane.showMessageDialog(frame
                ,"O campo \"Novo username\" n�o pode estar vazio.");
                txtUser.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                throw new Exception();
            }
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource().equals(buttonVoltar))
        {  
            dialog.setVisible(false);
            dialog.dispose();
        }
        if(e.getSource().equals(buttonSalvar))
        {
            buttonSalvar_Click();
        }
    }

    public void buttonSalvar_Click()
    {
        if(getNewUsername())
        {
            Users updateUser = antigoUser;
            updateUser.setUsername(txtUser.getText());
            try
            {
                controller.update(updateUser);
                JOptionPane.showMessageDialog(frame
                ,"Username alterado com sucesso.","Tudo certo!", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getOk());
                dialog.setVisible(false);
                dialog.dispose();
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(frame
                ,"N�o foi poss�vel alterar o username.");
            }
        }
    }
}
