package view.configs;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import view.panels.Fundos;
import view.FrameLogin;
import model.Database;
public class Deslogar
{
    Fundos fundo = new Fundos();
    JFrame frame;
    String appName;
    Database db;
    public Deslogar (JFrame frame, String n, Database db)
    {
        this.frame = frame;
        this.appName = n;
        this.db = db;
    }

    public void deslog()
    {
        int option = JOptionPane.showConfirmDialog(frame,
                "Deseja deslogar de sua conta?",
                appName    ,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,fundo.getExit());
        if((option == JOptionPane.YES_OPTION))
        {
            db.close();
            frame.setVisible(false);
            frame.dispose();
            String dbName = "users.db";
            String appName = "EasyTask";               
            db = new Database(dbName);
            FrameLogin frame = new FrameLogin(db, appName);
            frame.login();
        }
    }
}
