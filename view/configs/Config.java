package view.configs;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.SpringLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import model.Database;
import java.awt.Font;
import java.awt.Color;
import view.panels.Fundos;
import view.panels.Panel;
/**
 * Escreva a descri��o da classe Config aqui.
 * 
 * @author (seu nome) 
 * @version (n�mero de vers�o ou data)
 */
public class Config implements ActionListener
{
    private JButton buttonAltUser;
    private JButton buttonAltSenha;
    private JButton buttonExit;
    private JButton buttonDeslog;
    private JFrame frame;
    private JPanel panel;
    private JLabel titulo;
    private String appName;
    private Fundos fundo;
    private Database database;

    public Config(JFrame frame, String n, Database db)
    {
        this.frame = frame;
        this.appName = n;
        this.database = db;
        fundo = new Fundos();
    }

    public JPanel setPanel()
    {
        SpringLayout layout = new SpringLayout();
        panel = new Panel(fundo.getConfig());
        panel.setLayout(layout);

        titulo = new JLabel("Configura��es");
        titulo.setFont( new Font("SansSerif", Font.BOLD, 20));
        titulo.setForeground(Color.WHITE);
        panel.add(titulo);
        layout.putConstraint(SpringLayout.WEST,titulo,
            200,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH,titulo,
            7,
            SpringLayout.NORTH,panel);

        buttonAltUser = new JButton("Alterar Username");
        buttonAltUser.setOpaque(false);
        buttonAltUser.setForeground(Color.WHITE);
        buttonAltUser.setContentAreaFilled(false); 
        buttonAltUser.setBorderPainted(false);
        buttonAltUser.setFont( new Font("SansSerif", Font.BOLD, 13));
        panel.add(buttonAltUser);
        layout.putConstraint(SpringLayout.WEST,buttonAltUser,
            195,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH,buttonAltUser,
            80,
            SpringLayout.NORTH,panel);

        buttonAltSenha = new JButton("Alterar Senha");
        buttonAltSenha.setOpaque(false);
        buttonAltSenha.setForeground(Color.WHITE);
        buttonAltSenha.setContentAreaFilled(false); 
        buttonAltSenha.setBorderPainted(false);
        buttonAltSenha.setFont( new Font("SansSerif", Font.BOLD, 13));
        panel.add(buttonAltSenha);
        layout.putConstraint(SpringLayout.WEST,buttonAltSenha,
            207,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH,buttonAltSenha,
            10,
            SpringLayout.SOUTH,buttonAltUser);

        buttonDeslog = new JButton("Deslogar");
        buttonDeslog.setOpaque(false);
        buttonDeslog.setForeground(Color.WHITE);
        buttonDeslog.setContentAreaFilled(false);
        buttonDeslog.setBorderPainted(false);
        buttonDeslog.setFont( new Font("SansSerif", Font.BOLD, 13));
        panel.add(buttonDeslog);
        layout.putConstraint(SpringLayout.WEST,buttonDeslog,
            220,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH,buttonDeslog,
            10,
            SpringLayout.SOUTH,buttonAltSenha);

        buttonExit = new JButton("Sair");
        panel.add(buttonExit);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
            0 ,
            SpringLayout.EAST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonExit,
            0,
            SpringLayout.SOUTH,panel);

        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);  
        buttonExit.setForeground(Color.RED);

        buttonAltUser.addActionListener(this);
        buttonAltSenha.addActionListener(this);
        buttonDeslog.addActionListener(this);
        buttonExit.addActionListener(this);

        return panel;    
    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonExit))
        {
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {                       
                System.exit(0);
            }
        }

        if(e.getSource().equals(buttonAltUser))
        {
            AltUser alterarUsuario = new AltUser(frame, appName);
            alterarUsuario.setDialog();
        }

        if(e.getSource().equals(buttonAltSenha))
        {
            
            AltSenha alterarSenha = new AltSenha(frame, appName);
            alterarSenha.setDialog();
        }

        if(e.getSource().equals(buttonDeslog))
        {
            buttonDeslog_Click();

        }
    }

    public void buttonDeslog_Click()
    {
        Deslogar deslog = new Deslogar(frame, appName, database);
        deslog.deslog();
    }
}
