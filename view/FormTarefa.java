package view;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.Calendar;
import java.util.GregorianCalendar;
import controller.TarefaController;
import model.Database;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.entity.Tarefa;
import view.panels.Panel;
import view.panels.Fundos;
import view.format.*;
public class FormTarefa extends JFrame implements ActionListener
{
    private String appName;
    private TarefaController controle;
    private Database database;
    private List<Tarefa> tarefaLista;
    private int numTarefaCarregada;
    private boolean update;
    private boolean busca = false;

    private String id, labelsText [] = {"ID:","Nome:", "Descri��o:"," ","Data de Registro:","Data Final:", "Conclu�da"};
    private int numPairs;
    private JFrame frame;
    private Panel panel;
    private JTextField txtId;
    private JTextField txtName;
    private JTextField txtRgDate;
    private JTextField txtEndDate;
    private JTextArea txtDescri;
    private JCheckBox check;
    private JButton buttonCreate;
    private JButton buttonSave;
    private JButton buttonCancel;
    private JButton buttonReadPrevious;
    private JButton buttonReadNext;
    private JButton buttonUpdate;
    private JButton buttonDelete;
    private JButton buttonExit;
    private JButton buttonBuscar;
    private JButton buttonFrist;
    private JButton buttonLast;
    private JLabel label;
    private ArrayList<JButton> buttons = new ArrayList<JButton>();
    private ArrayList<JLabel> labels = new ArrayList<JLabel>();
    private ArrayList<JTextField> txt = new ArrayList<JTextField>();

    private Fundos fundo = new Fundos();

    public FormTarefa (Database database, String nome, JFrame dk)
    {
        numPairs = labelsText.length;
        this.frame = dk;
        this.appName = nome;
        this.controle = new TarefaController(database);
    }

    private void clearTextFields()
    {
        Component components[] = panel.getComponents();

        for (Component c: components)
        {
            if (c instanceof JTextField)
                ((JTextField) c).setText("");
        }   

        txtDescri.setText("");

    }

    private void showTarefa(int index)
    {
        Tarefa tarefa;
        if(index == -1)  
            clearTextFields();
        else
        {
            tarefa = tarefaLista.get(index);
            txtId.setText(Integer.toString(tarefa.getId()));
            txtName.setText(tarefa.getName());
            txtDescri.setText(tarefa.getDescri());
            txtEndDate.setText(tarefa.printEndDate());
            txtRgDate.setText(tarefa.printRegistrationDate());
            check.setSelected(tarefa.getComplet());
            locationTask(tarefa);
        }
    }

    private void locationTask(Tarefa tarefa)
    {
        buttonReadNext.setEnabled(true);
        buttonReadPrevious.setEnabled(true);
        buttonFrist.setEnabled(true);
        buttonLast.setEnabled(true);
        if(tarefa.getId() == tarefaLista.get(0).getId())
        {
            buttonFrist.setEnabled(false);
            buttonReadPrevious.setEnabled(false);
        }
        if(tarefa.getId() == tarefaLista.get(tarefaLista.size()-1).getId()) 
        {
            buttonLast.setEnabled(false);
            buttonReadNext.setEnabled(false);
        }
    }

    private void readTarefas()
    {
        try
        {
            this.tarefaLista = controle.readAll();
            if (tarefaLista.isEmpty()) 
                numTarefaCarregada = -1;
            else
                numTarefaCarregada = 0;
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(frame
            ,"Erro: N�o foi poss�vel ler. \nDetalhes:"+ e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }
    }

    private Tarefa getCurrentTarefaFromForm() throws Exception
    {
        Tarefa tarefa = new Tarefa();

        if (txtId.getText().trim().isEmpty() == false)
        {
            tarefa.setId(stringToInt(txtId.getText()));
        }

        if (txtName.getText().trim().isEmpty() == false)
        {
            tarefa.setName(txtName.getText());
        }

        if (txtDescri.getText().trim().isEmpty() == false)
        {
            tarefa.setDescri(txtDescri.getText());
        }

        if (txtRgDate.getText().trim().isEmpty() == false)
        {
            Calendar rgCalendar = Calendar.getInstance();
            tarefa.setRegistrationDate(rgCalendar.getTime());
        }

        if (txtEndDate.getText().trim().isEmpty() == false)
        {
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(stringToDate(txtEndDate.getText()));
            try
            {
                Calendar rgCalendar = Calendar.getInstance();

                endCalendar.add(Calendar.MONTH, -1);
                if(depoisQue(endCalendar,rgCalendar)==false)
                {
                    throw new Exception();
                }

                tarefa.setEndDate(endCalendar.getTime());
            }catch(Exception e)
            {
                throw new Exception("Data final inv�lida.");
            }

        }
        else 
        {
            throw new Exception("Insira a data final.");
        }

        tarefa.setComplet(check.isSelected());
        return tarefa;
    }

    //Utilit�rio para transformar um Date em uma string
    private java.util.Date stringToDate( String string ) throws Exception {
        Pattern dataPadrao = Pattern.compile("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");
        Matcher matcher = dataPadrao.matcher(string);
        if (matcher.matches()) {
            int dia = Integer.parseInt(matcher.group(1));
            int mes = Integer.parseInt(matcher.group(2));
            int ano = Integer.parseInt(matcher.group(3));
            return (new GregorianCalendar(ano,mes,dia)).getTime();
        }
        else throw new Exception("Data final inv�lida.");        
    }    

    public void adicionarButtons()
    {
        buttonCreate = new JButton(fundo.getNova());
        buttons.add(buttonCreate);

        buttonBuscar = new JButton(fundo.getPesquisa());
        buttons.add(buttonBuscar);

        buttonSave = new JButton(fundo.getSalvar());
        buttons.add(buttonSave);   
        buttonSave.setEnabled(false);

        buttonCancel = new JButton(fundo.getCancelar());
        buttons.add(buttonCancel);   
        buttonCancel.setEnabled(false);  

        buttonUpdate = new JButton(fundo.getAlterar());
        buttons.add(buttonUpdate);

        buttonDelete = new JButton(fundo.getExcluir());
        buttons.add(buttonDelete);

        //Segunda linha
        buttonFrist = new JButton(fundo.getPrimeiro());
        buttons.add(buttonFrist);

        buttonReadPrevious = new JButton(fundo.getPrevious());
        buttons.add(buttonReadPrevious);

        buttonReadNext = new JButton(fundo.getNext());
        buttons.add(buttonReadNext);

        buttonLast = new JButton(fundo.getUltimo());
        buttons.add(buttonLast);

        buttonExit = new JButton("Sair");
        buttons.add(buttonExit);

    }

    public void adicionarLabels()
    {   
        for(int i = 0; i< 7; i++)
        {
            label = new JLabel(labelsText[i]);  
            labels.add(label);

        }
    }

    public void adicionarTextField()
    {

        txtId = new JTextField(4);
        txt.add(txtId);
        txtName = new JTextField(30);
        txtName.setDocument( new Limite_digitos(30, "[^a-z|^A-Z|^0-9[.][ ][,]]"));
        txt.add(txtName);
        txtRgDate = new JTextField(10);
        txt.add(txtRgDate);
        txtEndDate = new JTextField(10);
        txt.add(txtEndDate);
    }

    public Panel createAndShowForm()
    {   
        panel = new Panel(fundo.getAnalise());
        SpringLayout layout = new SpringLayout();
        panel.setLayout(layout);
        adicionarButtons();
        adicionarLabels();
        adicionarTextField();
        int w = buttons.size();
        int z = 50;
        for(int i = 0; i < w ; i++)
        {
            if(i< w - 5)
            {
                buttons.get(i).setOpaque(false);
                buttons.get(i).setContentAreaFilled(false);
                buttons.get(i).setBorderPainted(false);
                panel.add(buttons.get(i));
                layout.putConstraint(SpringLayout.WEST, buttons.get(i),
                    69*i+z,
                    SpringLayout.WEST, panel);
                layout.putConstraint(SpringLayout.NORTH, buttons.get(i),
                    5,
                    SpringLayout.NORTH, panel);
            }
            else
            {
                if(i < w-1)
                {
                    buttons.get(i).setOpaque(false);
                    buttons.get(i).setContentAreaFilled(false);
                    buttons.get(i).setBorderPainted(false);
                    panel.add(buttons.get(i));
                    layout.putConstraint(SpringLayout.WEST, buttons.get(i),
                        69*(i-5)+z,
                        SpringLayout.WEST, panel);     
                    layout.putConstraint(SpringLayout.NORTH, buttons.get(i),
                        45,
                        SpringLayout.NORTH, buttons.get(i-5));
                }
                else
                {
                    buttons.get(i).setForeground(Color.RED);
                    buttons.get(i).setOpaque(false);
                    buttons.get(i).setContentAreaFilled(false);
                    buttons.get(i).setBorderPainted(false);
                    panel.add(buttons.get(i));
                    layout.putConstraint(SpringLayout.EAST, buttons.get(i),
                        0 ,
                        SpringLayout.EAST, panel);     
                    layout.putConstraint(SpringLayout.SOUTH, buttons.get(i),
                        0,
                        SpringLayout.SOUTH,panel);
                }
            }
        }

        w = txt.size();
        for(int i = 0; i < w; i++)
        {
            if(i < 2)
            {
                txt.get(i).setOpaque(false);
                txt.get(i).setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
                panel.add(txt.get(i)); 
                layout.putConstraint(SpringLayout.WEST, txt.get(i),
                    10,
                    SpringLayout.EAST, labels.get(i));
                layout.putConstraint(SpringLayout.NORTH, txt.get(i),
                    25*i+100,
                    SpringLayout.NORTH, panel);
            } 

            else
            {  
                txt.get(i).setOpaque(false);
                txt.get(i).setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
                panel.add(txt.get(i));
                layout.putConstraint(SpringLayout.WEST, txt.get(i),
                    10,
                    SpringLayout.EAST, labels.get(i+2));
                layout.putConstraint(SpringLayout.NORTH, txt.get(i),
                    25*(i+2)+100,
                    SpringLayout.NORTH, panel);
            }
        }

        w = labels.size();
        for(int i = 0; i < w; i++)
        {
            if(i < w-1)
            {
                panel.add(labels.get(i));
                layout.putConstraint(SpringLayout.WEST, labels.get(i),
                    20,
                    SpringLayout.WEST, panel);
                layout.putConstraint(SpringLayout.NORTH, labels.get(i),
                    25*i+100,
                    SpringLayout.NORTH, panel);
            }
            else
            {
                panel.add(labels.get(i));
                layout.putConstraint(SpringLayout.WEST, labels.get(i),
                    245,
                    SpringLayout.WEST, panel);
                layout.putConstraint(SpringLayout.NORTH, labels.get(i),
                    25*i+100,
                    SpringLayout.NORTH, panel);
            }
        }

        txtDescri = new JTextArea(1,1);
        txtDescri.setLineWrap(true);
        txtDescri.setWrapStyleWord(true);
        txtDescri.setDocument( new Limite_digitos(95,"[^a-z|^A-Z|^0-9[.][ ][,]]"));

        panel.add(txtDescri);
        layout.putConstraint(SpringLayout.WEST, txtDescri,
            10,
            SpringLayout.EAST, labels.get(2));
        layout.putConstraint(SpringLayout.NORTH, txtDescri,
            150,
            SpringLayout.NORTH, panel);
        layout.putConstraint(SpringLayout.EAST, txtDescri,
            -100,
            SpringLayout.EAST, panel);
        layout.putConstraint(SpringLayout.SOUTH, txtDescri,
            190,
            SpringLayout.NORTH, panel);

        check = new JCheckBox("");
        check.setOpaque(false);
        panel.add(check);
        layout.putConstraint(SpringLayout.EAST, check ,
            -3,
            SpringLayout.WEST, labels.get((labels.size()-1)));
        layout.putConstraint(SpringLayout.NORTH, check,
            25*(labels.size()-1)+98,
            SpringLayout.NORTH, panel);

        // Associando eventos aos botoes
        buttonCreate.addActionListener(this);
        buttonSave.addActionListener(this);
        buttonCancel.addActionListener(this);
        buttonFrist.addActionListener(this);
        buttonReadPrevious.addActionListener(this);
        buttonReadNext.addActionListener(this);
        buttonLast.addActionListener(this);
        buttonUpdate.addActionListener(this);
        buttonDelete.addActionListener(this);
        buttonExit.addActionListener(this);
        buttonBuscar.addActionListener(this);

        txtRgDate.setEditable(false);
        txtEndDate.setEditable(false);
        txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
        txtName.setEditable(false);
        txtName.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
        txtDescri.setEditable(false);
        txtDescri.setOpaque(false);
        txtId.setEditable(false);
        check.setEnabled(false);
        readTarefas();
        if(numTarefaCarregada == -1) 
        {
            buttonReadPrevious.setEnabled(false);
            buttonReadNext.setEnabled(false);
            buttonUpdate.setEnabled(false);
            buttonDelete.setEnabled(false);
            buttonBuscar.setEnabled(false);
            buttonBuscar.setEnabled(false);
            buttonFrist.setEnabled(false);
            buttonLast.setEnabled(false);
        }
        showTarefa(numTarefaCarregada);     
        return panel;
    }

    public boolean depoisQue(Calendar date1, Calendar date2)
    {
        if(date1.get(Calendar.YEAR) > date2.get(Calendar.YEAR)) return true;
        if(date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR))
        {
            if(date1.get(Calendar.MONTH)  > date2.get(Calendar.MONTH)) return true;
            if(date1.get(Calendar.MONTH) == date2.get(Calendar.MONTH))
                if(date1.get(Calendar.DAY_OF_MONTH)  >= date2.get(Calendar.DAY_OF_MONTH)) return true;
        }
        return false;
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(buttonCreate))
        {
            buttonCreate_Click();
        }

        if(e.getSource().equals(buttonSave))
        {
            buttonSave_Click();
        }

        if(e.getSource().equals(buttonCancel))
        {
            buttonCancel_Click();
        }        

        if(e.getSource().equals(buttonFrist))
        {
            buttonFrist_Click();
        }

        if(e.getSource().equals(buttonReadPrevious))
        {
            buttonReadPrevious_Click();
        }

        if(e.getSource().equals(buttonReadNext))
        {
            buttonReadNext_Click();
        }

        if(e.getSource().equals(buttonLast))
        {
            buttonLast_Click();
        }

        if(e.getSource().equals(buttonUpdate))
        {
            buttonUpdate_Click();
        }

        if(e.getSource().equals(buttonDelete))
        {
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Dseja excluir a tarefa atual?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, fundo.getExcluir());

            if (option == JOptionPane.YES_OPTION)
            {           
                buttonDelete_Click();
            }            
        }

        if(e.getSource().equals(buttonExit))
        {
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {   
                System.exit(0);
            }
        }

        if(e.getSource().equals(buttonBuscar))
        {
            clearTextFields();
            buttonBuscar_Click();
        }

    } 

    private int stringToInt( String string ) {
        return string.equals("") ? 0 : Integer.parseInt(string);
    }

    private void buttonBuscar_Click()
    {
        buttonCreate.setEnabled(false);
        buttonSave.setEnabled(true);
        buttonCancel.setEnabled(true);
        buttonFrist.setEnabled(false);
        buttonLast.setEnabled(false);
        buttonReadPrevious.setEnabled(false);
        buttonReadNext.setEnabled(false);
        buttonUpdate.setEnabled(false);
        buttonDelete.setEnabled(false);
        txtId.setEditable(true);
        txtId.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        txtName.setEditable(false);
        txtName.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
        txtEndDate.setEditable(false);
        txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        txtDescri.setEditable(false);
        txtDescri.setOpaque(false);
        busca = true;    

    }

    private void buttonCreate_Click()
    {
        clearTextFields(); 
        update = false;
        buttonBuscar.setEnabled(false);
        buttonCreate.setEnabled(false);
        buttonSave.setEnabled(true);
        buttonCancel.setEnabled(true);
        buttonFrist.setEnabled(false);
        buttonLast.setEnabled(false);
        buttonReadPrevious.setEnabled(false);
        buttonReadNext.setEnabled(false);
        buttonUpdate.setEnabled(false);
        buttonDelete.setEnabled(false);

        if(numTarefaCarregada == 0)
            txtId.setText(String.valueOf(tarefaLista.get(tarefaLista.size()-1).getId() + 1));

        txtName.setEditable(true);
        txtName.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        txtDescri.setEditable(true);
        txtDescri.setOpaque(true);
        txtEndDate.setEditable(true);
        txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        check.setSelected(false);
        check.setEnabled(false);
        SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        txtRgDate.setText(dateFor.format(calendar.getTime()));

    }      

    private void buttonSave_Click()
    {
        if(busca)
        {
            try
            { 
                busca = false;
                Scanner test = new Scanner(txtId.getText().trim());
                if(test.hasNextInt())
                {
                    Tarefa tarefa = controle.loadFromId(stringToInt(txtId.getText()));
                    txtId.setText(Integer.toString(tarefa.getId()));
                    txtName.setText(tarefa.getName());
                    txtEndDate.setText(tarefa.printEndDate());
                    txtRgDate.setText(tarefa.printRegistrationDate());
                    check.setSelected(tarefa.getComplet());
                    buttonCreate.setEnabled(true);
                    buttonSave.setEnabled(false);
                    buttonBuscar.setEnabled(true);
                    buttonCancel.setEnabled(false);
                    buttonReadPrevious.setEnabled(true);
                    buttonFrist.setEnabled(true);
                    buttonLast.setEnabled(true);
                    buttonReadNext.setEnabled(true);
                    buttonUpdate.setEnabled(true);
                    buttonDelete.setEnabled(true);
                    locationTask(tarefa);
                    txtId.setEditable(false);
                    txtId.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
                }
                else
                {
                    JOptionPane.showMessageDialog(frame
                    ,"Argumento inv�lido. \nPor favor, insira um n�mero inteiro! ","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                    txtId.setText("");
                    buttonBuscar_Click();
                }
            }
            catch(Exception e)
            {
                buttonBuscar_Click();
                txtId.setText("");
                JOptionPane.showMessageDialog(frame
                ,"Tarefa n�o encontrada. \nDetalhes: N�o h� nenhuma tarefa com esse Id","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
            }    
        }
        else
        {   
            try
            {
                Tarefa tarefa = getCurrentTarefaFromForm();
                if (update && tarefa.getName() != null) 
                {
                    int option;

                    option = JOptionPane.showConfirmDialog(frame, "Deseja alterar a tarefa atual?", appName,
                        JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,fundo.getSalvar());

                    if (option == JOptionPane.YES_OPTION)
                    {
                        controle.update(tarefa);
                        buttonCreate.setEnabled(true);
                        buttonSave.setEnabled(false);
                        buttonBuscar.setEnabled(true);
                        buttonCancel.setEnabled(false);
                        buttonReadPrevious.setEnabled(true);
                        buttonFrist.setEnabled(true);
                        buttonLast.setEnabled(true);
                        buttonReadNext.setEnabled(true);
                        buttonUpdate.setEnabled(true);
                        buttonDelete.setEnabled(true);
                        check.setEnabled(false);
                        txtName.setEditable(false);
                        txtName.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
                        txtEndDate.setEditable(false);
                        txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
                        txtDescri.setEditable(false);
                        txtDescri.setOpaque(false);
                        readTarefas();
                        showTarefa(numTarefaCarregada);

                        update = false;
                    }
                }
                else
                {
                    if(update)  throw new Exception("O campo \"Nome\" n�o pode estar vazio"); 
                    else 
                    {
                        if(tarefa.getName() != null)
                        {
                            controle.create(tarefa);
                        }
                        else
                        {
                            throw new Exception("O campo \"Nome\" n�o pode estar vazio");
                        } 
                    }
                    buttonCreate.setEnabled(true);
                    buttonSave.setEnabled(false);
                    buttonBuscar.setEnabled(true);
                    buttonCancel.setEnabled(false);
                    buttonReadPrevious.setEnabled(true);
                    buttonFrist.setEnabled(true);
                    buttonLast.setEnabled(true);
                    buttonReadNext.setEnabled(true);
                    buttonUpdate.setEnabled(true);
                    buttonDelete.setEnabled(true);
                    check.setEnabled(false);
                    txtName.setEditable(false);
                    txtName.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
                    txtDescri.setEditable(false);
                    txtDescri.setOpaque(false);
                    txtEndDate.setEditable(false);
                    txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
                    readTarefas();
                    showTarefa(numTarefaCarregada);
                }
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(frame
                ,"ERROR: N�o foi poss�vel salvar a tarefa. \nDetalhes: "+ e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
            }
        } 
    }

    private void buttonCancel_Click()
    {
        if(numTarefaCarregada == -1)
        {               
            buttonCreate.setEnabled(true);
            buttonSave.setEnabled(false);
            buttonCancel.setEnabled(false);
            clearTextFields();
            txtName.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
            txtName.setEditable(false);
            txtDescri.setEditable(false);
            txtDescri.setOpaque(false);
            txtEndDate.setEditable(false);
            txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));

        }  
        else
        {
            clearTextFields();
            buttonCreate.setEnabled(true);
            buttonBuscar.setEnabled(true);
            buttonSave.setEnabled(false);
            buttonCancel.setEnabled(false);
            buttonReadPrevious.setEnabled(true);
            buttonFrist.setEnabled(true);
            buttonLast.setEnabled(true);
            buttonReadNext.setEnabled(true);
            buttonUpdate.setEnabled(true);
            buttonDelete.setEnabled(true);
            txtName.setEditable(false);
            txtName.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
            txtDescri.setEditable(false);
            txtDescri.setOpaque(false);
            txtEndDate.setEditable(false);
            txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0,0 , 0, Color.WHITE));
            txtId.setEditable(false);
            txtId.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
            check.setEnabled(false);
            busca = false;
            update = false;
            readTarefas();
            showTarefa(numTarefaCarregada);
        }
    }      

    private void buttonFrist_Click()
    {
        showTarefa(0);
    }

    private void buttonReadNext_Click()
    {
        if ((numTarefaCarregada+1) < tarefaLista.size())
        {
            numTarefaCarregada++;
            showTarefa(numTarefaCarregada);
        }
    }

    private void buttonReadPrevious_Click()
    {
        if ((numTarefaCarregada-1) >= 0)
        {
            numTarefaCarregada--;
            showTarefa(numTarefaCarregada);
        }        
    }

    private void buttonLast_Click()
    {
        showTarefa(tarefaLista.size()-1);
    }

    private void buttonUpdate_Click()
    {

        Tarefa tarefa = new Tarefa();
        check.setEnabled(true);
        buttonBuscar.setEnabled(false);
        txtName.setEditable(true);
        txtName.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        txtDescri.setEditable(true);
        txtDescri.setOpaque(true);
        txtEndDate.setEditable(true);
        txtEndDate.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        buttonCreate.setEnabled(false);
        buttonSave.setEnabled(true);
        buttonCancel.setEnabled(true);
        buttonFrist.setEnabled(false);
        buttonLast.setEnabled(false);
        buttonReadPrevious.setEnabled(false);
        buttonReadNext.setEnabled(false);
        buttonUpdate.setEnabled(false);
        buttonDelete.setEnabled(false);
        update = true;
    }

    private void buttonDelete_Click()
    {
        try
        {
            Tarefa tarefa = getCurrentTarefaFromForm();
            controle.delete(tarefa);        
            readTarefas();
            showTarefa(numTarefaCarregada); 
            if(numTarefaCarregada == -1)
            {               
                buttonReadPrevious.setEnabled(false);
                buttonReadNext.setEnabled(false);
                buttonUpdate.setEnabled(false);
                buttonDelete.setEnabled(false);
                buttonBuscar.setEnabled(false);
                buttonBuscar.setEnabled(false);
                buttonFrist.setEnabled(false);
                buttonLast.setEnabled(false);          
            }   
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(frame,"ERROR: N�o foi poss�vel deletar a tarefa. \nDETALHE: "
                +e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }           
    }   
}
