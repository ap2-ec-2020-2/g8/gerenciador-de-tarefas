package view.panels;

import view.Frame;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import model.Database;
import java.awt.Point;
import view.Frame;
import model.entity.Users;
import controller.UsersController;
import view.format.Limite_digitos;
public class PanelLogin extends JPanel implements ActionListener
{
    private Fundos fundo = new Fundos();
    private JButton buttonExit;
    private JButton buttonLogin;
    private JButton buttonCadastrar;
    private JLabel username;
    private JLabel senha;
    private JFrame frame;
    private UsersController controller;
    private PanelCadastro panelCadastro;
    private JPanel panel;
    private Database database;
    private Database databaseTask;
    private JTextField txtUsername;
    private JPasswordField txtSenha;
    private String appName;
    public int entrar = -1;

    public PanelLogin (Database db, String n)
    {
        this.database = db;
        this.appName = n;
        controller = new UsersController(db);
    }

    public void paintComponent(Graphics g)
    {   
        super.paintComponent(g);
        g.drawImage(fundo.getLogin().getImage(), 0, 0, this);
    }

    public JPanel alterarPanel(JFrame frame)
    {
        this.frame = frame;

        panel = new PanelLogin(database, appName);
        username = new JLabel("Username:");
        username.setForeground(Color.WHITE);
        username.setFont( new Font("Dialog", Font.BOLD, 13));
        senha = new JLabel("Senha:");
        senha.setFont( new Font("Dialog", Font.BOLD, 13));
        senha.setForeground(Color.WHITE);

        txtUsername  = new JTextField(11);
        txtUsername.setForeground(Color.WHITE);
        txtUsername.setFont( new Font("Dialog", Font.BOLD, 13));
        txtUsername.setDocument( new Limite_digitos(13,"[^a-z|^A-Z|^0-9[ ]]"));
        txtSenha  = new JPasswordField(10);
        txtSenha.setDocument( new Limite_digitos(13, ""));
        txtSenha.setForeground(Color.WHITE);
        txtSenha.setDocument( new Limite_digitos(11,""));
        txtSenha.setFont( new Font("Dialog", Font.BOLD, 13));

        buttonLogin = new JButton("Entrar");
        buttonLogin.setFont( new Font("Dialog", Font.BOLD, 13));

        buttonExit = new JButton("Sair");
        buttonExit.setFont( new Font("Dialog", Font.BOLD, 13));

        buttonCadastrar = new JButton("Cadastrar");
        buttonCadastrar.setFont( new Font("Dialog", Font.BOLD, 13));

        SpringLayout layout = new SpringLayout();
        panel.setLayout(layout);

        panel.add(username);
        layout.putConstraint(SpringLayout.WEST, username,
            45,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, username,
            35,
            SpringLayout.NORTH,panel);

        panel.add(senha);                            
        layout.putConstraint(SpringLayout.WEST, senha,
            68,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, senha,
            65,
            SpringLayout.NORTH,panel);

        panel.add(txtUsername);
        txtUsername.setOpaque(false);
        txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        layout.putConstraint(SpringLayout.WEST, txtUsername,
            5,
            SpringLayout.EAST, username);     
        layout.putConstraint(SpringLayout.NORTH,txtUsername,
            35,
            SpringLayout.NORTH,panel);

        panel.add(txtSenha);
        txtSenha.setOpaque(false);
        txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        layout.putConstraint(SpringLayout.WEST, txtSenha,
            5,
            SpringLayout.EAST, senha);     
        layout.putConstraint(SpringLayout.NORTH, txtSenha,
            64,
            SpringLayout.NORTH,panel);

        panel.add(buttonLogin);
        layout.putConstraint(SpringLayout.WEST, buttonLogin,
            117,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, buttonLogin,
            95,
            SpringLayout.NORTH,panel);
        buttonLogin.setOpaque(false);
        buttonLogin.setBorderPainted(false);
        buttonLogin.setContentAreaFilled(false);
        buttonLogin.setForeground(Color.WHITE);

        panel.add(buttonCadastrar);
        layout.putConstraint(SpringLayout.WEST, buttonCadastrar,
            110,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, buttonCadastrar,
            5,
            SpringLayout.SOUTH,buttonLogin);
        buttonCadastrar.setOpaque(false);
        buttonCadastrar.setBorderPainted(false);
        buttonCadastrar.setContentAreaFilled(false);
        buttonCadastrar.setForeground(Color.WHITE);

        cadatrados();
        panel.add(buttonExit);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
            0 ,
            SpringLayout.EAST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonExit,
            0,
            SpringLayout.SOUTH,panel);
        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);
        buttonExit.setForeground(Color.RED);

        buttonCadastrar.addActionListener(this);
        buttonLogin.addActionListener(this); 
        buttonExit.addActionListener(this); 
        clearTextFields(); 

        return panel;
    }

    public void cadatrados()
    {
        try
        {
            if(controller.qntRegistros() == 0)
            {
                buttonLogin.setEnabled(false);
                txtUsername.setEnabled(false);
                txtSenha.setEnabled(false);
            }
        }
        catch(Exception e)
        {
            System.err.println("N�o foi poss�vel contar os registros");
        }
    }

    private void clearTextFields()
    {
        Component components[] = panel.getComponents();

        for (Component c: components)
        {
            if (c instanceof JTextField)
                ((JTextField) c).setText("");
        }   
    }

    public Users getDadosLogin() throws Exception
    {
        try
        {
            Users username = new Users();
            txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
            if(txtUsername.getText().trim().isEmpty() == false )
            {
                username.setUsername(txtUsername.getText());
                if(txtSenha.getText().trim().isEmpty() == false)
                {
                    username.setSenha(txtSenha.getText());
                    return username;
                }
                else 
                {
                    txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                    throw new Exception("Insira a senha.");
                }
            }
            else
            {
                txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                throw new Exception("Insira o username.");
            }
        }catch(Exception e)
        {
            throw new Exception(e.getMessage());
        }

    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonExit))
        {
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName    ,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {                       
                System.exit(0);
            }
        }

        if (e.getSource().equals(buttonLogin))
        { 
            buttonLogin_Click();
        }

        if (e.getSource().equals(buttonCadastrar))
        { 
            buttonCadastrar_Click(); 
        }
    }  

    public void buttonLogin_Click()
    {
        int id=-1;
        Users tentaUsername;
        try
        {
            
            tentaUsername = getDadosLogin();
            id = controller.loadUsers(tentaUsername.getUsername(), tentaUsername.getSenha());
            Point xy = frame.getLocation();
            frame.setVisible(false);
            frame.dispose();
            String dbName = "Tarefas"+String.valueOf(id);
            database.close();
            databaseTask = new Database(dbName);
            Frame framePanels = new Frame(databaseTask, dbName, xy);
        }
        catch(Exception e)
        {
            if(id == -1)
            {
                clearTextFields(); 
                txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                JOptionPane.showMessageDialog(frame,e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
            }
            else
                JOptionPane.showMessageDialog(frame,e.getMessage(),"Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
        }

    }

    public void buttonCadastrar_Click()
    {
        panelCadastro = new  PanelCadastro(database, appName, frame);
        frame.getContentPane().removeAll();
        frame.add(panelCadastro.alterarPanelCadastro());
        frame.pack();
        frame.setSize(323,210);
        frame.setVisible(true);
    }
}  