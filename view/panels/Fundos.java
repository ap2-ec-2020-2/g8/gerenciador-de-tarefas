package view.panels;
import javax.swing.*;

/**
 * Escreva a descri��o da classe fundos aqui.
 * 
 * @author (seu nome) 
 * @version (n�mero de vers�o ou data)
 */
public class Fundos
{
    private ImageIcon login = new ImageIcon("FundoLogin.jpeg");
    private ImageIcon inicio = new ImageIcon("FundoInicio.jpeg");
    private ImageIcon sobre = new ImageIcon("FundoSobre.jpeg");
    private ImageIcon analise = new ImageIcon("FundoAnalise.jpeg");
    private ImageIcon lista = new ImageIcon("FundoLista.jpeg");
    private ImageIcon config = new ImageIcon("FundoConfig.png");
    private ImageIcon alt = new ImageIcon("FundoAlt.jpeg");
    private ImageIcon icon = new ImageIcon("icone.jpeg");
    private ImageIcon exit = new ImageIcon("Sair.png");
    private ImageIcon save = new ImageIcon("Salvar.png");
    private ImageIcon excluir = new ImageIcon("excluir.png");
    private ImageIcon next = new ImageIcon("proximo.png");
    private ImageIcon ultimo = new ImageIcon("ultimo.png");
    private ImageIcon previous = new ImageIcon("previous.png");
    private ImageIcon pesquisa = new ImageIcon("pesquisa.png");
    private ImageIcon alterar = new ImageIcon("alterar.png");
    private ImageIcon cancelar = new ImageIcon("cancelar.png");
    private ImageIcon primeiro = new ImageIcon("primeiro.png");
    private ImageIcon nova = new ImageIcon("add.png");
    private ImageIcon help = new ImageIcon("FundoHelp.jpeg");
    private ImageIcon atencao = new ImageIcon("atencao.png");
    private ImageIcon ok = new ImageIcon("ok.png");
    
    /**GET Method Propertie fundoIniciofundo*/
    public ImageIcon getAlterar(){
        return this.alterar;
    }//end method getPesquisa
    
    /**GET Method Propertie fundoIniciofundo*/
    public ImageIcon getPesquisa(){
        return this.pesquisa;
    }//end method getPesquisa
    
    /**GET Method Propertie fundoIniciofundo*/
    public ImageIcon getInicio(){
        return this.inicio;
    }//end method getInicio

    /**GET Method Propertie login*/
    public ImageIcon getLogin(){
        return this.login;
    }//end method getLogin

    public ImageIcon getNext(){
        return this.next;
    }//end method getNext
    
    
    public ImageIcon getPrevious(){
        return this.previous;
    }//end method getPrevious
    
    /**GET Method Propertie sobre*/
    public ImageIcon getSobre(){
        return this.sobre;
    }//end method getSobre

    /**GET Method Propertie analise*/
    public ImageIcon getAnalise(){
        return this.analise;
    }//end method getAnalise

    /**GET Method Propertie analise*/
    public ImageIcon getLista(){
        return this.lista;
    }//end method getLista
    
    /**GET Method Propertie icon*/
    public ImageIcon getIcon(){
        return this.icon;
    }//end method getIcon
    
    /**GET Method Propertie icon*/
    public ImageIcon getExit(){
        return this.exit;
    }//end method getExit
    
    /**GET Method Propertie icon*/
    public ImageIcon getSalvar(){
        return this.save;
    }//end method getSalvar
    
    /**GET Method Propertie icon*/
    public ImageIcon getExcluir(){
        return this.excluir;
    }//end method getExcluir

    /**GET Method Propertie cancelar*/
    public ImageIcon getCancelar(){
        return this.cancelar;
    }//end method getCancelar
 
    /**GET Method Propertie ultimo*/
    public ImageIcon getUltimo(){
        return this.ultimo;
    }//end method getUltimo

//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie nova*/
    public ImageIcon getNova(){
        return this.nova;
    }//end method getNova



    //Start GetterSetterExtension Source Code
    /**GET Method Propertie primeiro*/
    public ImageIcon getPrimeiro(){
        return this.primeiro;
    }//end method getPrimeiro

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie config*/
    public ImageIcon getConfig(){
        return this.config;
    }//end method getConfig

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie alt*/
    public ImageIcon getAlt(){
        return this.alt;
    }//end method getAlt

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie help*/
    public ImageIcon getHelp(){
        return this.help;
    }//end method getHelp

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie atencao*/
    public ImageIcon getAtencao(){
        return this.atencao;
    }//end method getAtencao

    //End GetterSetterExtension Source Code
//!

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie ok*/
    public ImageIcon getOk(){
        return this.ok;
    }//end method getOk

    //End GetterSetterExtension Source Code
//!
}
