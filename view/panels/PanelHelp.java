package view.panels;

import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.Font;
import view.Utils;
import java.awt.Dimension;
public class PanelHelp extends JPanel implements ActionListener
{
    private Fundos fundo = new Fundos();   
    private JFrame frame;
    private JPanel panel;
    private Utils util = new Utils();
    private JButton buttonExit;
    private JLabel titulo;
    private JTextArea txt;
    private SpringLayout layout;
    String appName;
    public void paintComponent(Graphics g)
    {   
        super.paintComponent(g);
        g.drawImage(fundo.getSobre().getImage(), 0, 0, this);
    }

    public JPanel setPanel(JFrame frame)
    {
        this.frame = frame;
        panel =  new Panel(fundo.getHelp());
        
        txt= new JTextArea();
        txt.setLineWrap(true);
        txt.setWrapStyleWord(true);
        txt.setEditable(false);
        
        layout = new SpringLayout();
        panel.setLayout(layout);
        JScrollPane scrollPane = new JScrollPane(txt); 
        scrollPane.setPreferredSize(new Dimension(200, 200));
    

        buttonExit = new JButton("Sair");

        titulo = new JLabel("Ajuda ao usu�rio");

        titulo.setFont(new Font("Dialog", Font.BOLD, 15));
        
        
        txt.setText("          O que � o EasyTask?\n"+

"\n          O EasyTask nada mais � do que um aplicativo desenvolvido por estudantes de engenharia"+" que desejavam achar um m�todo"+
"organizacional simples para suas atividades acad�micas"+

"\n\n\n          Como salvar uma nova tarefa?\n"+

"\n          Para salvar uma nova tarefa, o usu�rio deve criar um cadastro pr�vio, com seu login e senha."+
" Ap�s isso, entrar� no menu TAREFAS, e clicar� no primeiro �cone, adicionando �nome�, �descri��o� e �data final�"+
". Em seguida, para salvar sua tarefa, clicar� no terceiro �cone (confirma��o)."+

"\n\n\n          Como editar uma tarefa j� existente?\n"+

"\n          Para realizar qualquer altera��o em uma tarefa j� existente basta que o usu�rio clique no segundo �cone (lupa),"+
" selecione o ID da tarefa desejada, e depois no quinto �cone, com isso, realizar� a altera��o que julgar necess�ria."+
" Ap�s isso, basta clicar no terceiro �cone(confirma��o)."+
 
"\n\n\n          Como marcar uma tarefa como conclu�da?\n"+

"\n          Para marcar uma tarefa como conclu�da basta que, ap�s realizar o login, o usu�rio v� at� o"+
" menu �CADASTRO�, v� at� a tarefa desejada, clique no quadradinho respectivo"+" a sua tarefa e clique no bot�o �CONCLUIR�."+

"\n\n\n          Como apagar uma tarefa?\n"+

"\n          Para apagar uma tarefa, basta seguir os passos"+
" anteriores e clicar em �APAGAR�."+

"\n\n\n          Como listar todas as tarefas?\n"+

"\n          Para listar todas tarefas, seja para marcar ou desmarcar,"+
" basta seguir os passos anteriores e clicar em �MARCAR/ DESMARCAR TODOS�."+

"\n\n\n          Como alterar o username?\n"+

"\n          Para alterar o username, o usu�rio deve estar logado, uma vez logado,"+
" ir na op��o �CONFIG.� na barra de menu. Ap�s isso, ir na op��o �Alterar Username�,"+
" na qual ele dever� colocar o novo username desejado no campo �Novo username�,"+
 "depois escrev�-lo novamente em �Confirme�. E para finalizar clique em �Salvar�"+
 ", e assim ele ter� o seu novo username salvo."+

"\n\n\n         Como alterar a senha?\n"+

"\n          Para alterar a senha, o usu�rio dever� estar logado,"+
" uma vez logado, ir na op��o �CONFIG.� na barra de menu."+
" Ap�s isso, ir na op��o �Alterar Senha�, na qual o usu�rio "+
"dever� colocar primeiramente sua senha atual, depois sua nova senha "+
"e por �ltimo dever� confirmar a nova senha, digitando-a novamente,"+
" e clicar em �Salvar�. Assim ele ter� a sua nova senha salva."+

"\n\n\n          Como deslogar de sua conta?\n"+

"\n          Para deslogar, o usu�rio dever� ir na op��o �CONFIG.�,"+
" posteriormente ir na op��o �deslogar�, e assim depois do usu�rio confirmar,"+
" ele n�o estar� logado em sua conta e retornar� para a tela de login.");

        txt.setFont(new Font("SansSerif", Font.PLAIN, 13));
        txt.setOpaque(false);
        txt.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        
        buttonExit.addActionListener(this);        

        
        JViewport viewport = new JViewport();
        viewport.setView(txt);
        viewport.setOpaque(false);
        scrollPane.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        scrollPane.setOpaque(false);
        scrollPane.setViewport(viewport);
        scrollPane.setWheelScrollingEnabled(true);
        panel.add(scrollPane);
        panel.add(titulo);

        layout.putConstraint(SpringLayout.WEST, titulo,
            210 ,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, titulo,
            10,
            SpringLayout.NORTH,panel);

        layout.putConstraint(SpringLayout.NORTH, scrollPane,
            50 ,
            SpringLayout.NORTH, panel);               
        layout.putConstraint(SpringLayout.WEST, scrollPane,
            40 ,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, scrollPane,
            -30,
            SpringLayout.SOUTH,panel);
        layout.putConstraint(SpringLayout.EAST,scrollPane,
            -30,
            SpringLayout.EAST,panel);              

       
        panel.add(buttonExit);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
            0 ,
            SpringLayout.EAST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonExit,
            0,
            SpringLayout.SOUTH,panel);
        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);
        buttonExit.setForeground(Color.RED);
        return panel;
    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonExit))
        {               
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {                       
                System.exit(0);
            }
        }


    }
}