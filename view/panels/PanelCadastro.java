
package view.panels;

import view.Frame;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.SpringLayout;
import model.Database;
import model.entity.Users;
import controller.UsersController;
import view.format.Limite_digitos;
public class PanelCadastro extends JPanel implements ActionListener
{
    private Fundos fundo = new Fundos();
    private JFrame frame;
    private JButton buttonExit;
    private JButton buttonVoltar;
    private JButton buttonCadastrar;
    private JPanel panel;
    private UsersController controller;
    private Users usernameCadastro = new Users();
    private JLabel username;
    private JLabel senha;
    private JLabel confirm;
    private Database database;
    private JTextField txtUsername;
    private JPasswordField txtSenha;
    private JPasswordField txtConfirm;
    private String appName;

    public PanelCadastro (Database db, String n, JFrame frame)
    {
        this.database = db;
        this.appName = n;
        this.frame = frame;
        controller = new UsersController(db);
    }

    public void paintComponent(Graphics g)
    {   
        super.paintComponent(g);
        g.drawImage(fundo.getLogin().getImage(), 0, 0, this);
    }

    public JPanel alterarPanelCadastro()
    {
        panel = new PanelCadastro(database,appName, frame);
        SpringLayout layout = new SpringLayout();
        panel.setLayout(layout);  
        buttonExit = new JButton("Sair");
        buttonExit.setFont( new Font("Dialog", Font.BOLD, 13));
        buttonVoltar = new JButton("Voltar");
        buttonVoltar.setFont( new Font("Dialog", Font.BOLD, 13));
        buttonCadastrar = new JButton("Cadastrar-me");
        buttonCadastrar.setFont( new Font("Dialog", Font.BOLD, 13));

        username = new JLabel("Username:");
        username.setFont( new Font("Dialog", Font.BOLD, 13));
        username.setForeground(Color.WHITE);
        senha = new JLabel("Senha:");
        senha.setFont( new Font("Dialog", Font.BOLD, 13));
        senha.setForeground(Color.WHITE);
        confirm = new JLabel("Confirme:");
        confirm.setFont( new Font("Dialog", Font.BOLD, 13));
        confirm.setForeground(Color.WHITE);

        txtUsername  = new JTextField(11);
        txtUsername.setForeground(Color.WHITE);
        txtUsername.setFont( new Font("Dialog", Font.BOLD, 13));
        txtUsername.setDocument( new Limite_digitos(13, "[^a-z|^A-Z|^0-9[ ]]"));
        txtSenha  = new JPasswordField(10);
        txtSenha.setFont( new Font("Dialog", Font.BOLD, 13));
        txtSenha.setForeground(Color.WHITE);
        txtSenha.setDocument( new Limite_digitos(11,""));
        txtConfirm = new JPasswordField(10);
        txtConfirm.setFont( new Font("Dialog", Font.BOLD, 13));
        txtConfirm.setDocument( new Limite_digitos(11,""));
        txtConfirm.setForeground(Color.WHITE);

        panel.add(username);
        layout.putConstraint(SpringLayout.WEST, username,
            60,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, username,
            15,
            SpringLayout.NORTH,panel);

        panel.add(senha);                            
        layout.putConstraint(SpringLayout.WEST, senha,
            80,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, senha,
            45,
            SpringLayout.NORTH,panel);

        panel.add(confirm);                            
        layout.putConstraint(SpringLayout.WEST, confirm,
            60,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, confirm,
            75,
            SpringLayout.NORTH,panel);    

        panel.add(txtUsername);

        txtUsername.setOpaque(false);
        txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        layout.putConstraint(SpringLayout.WEST, txtUsername,
            5,
            SpringLayout.EAST, username);     
        layout.putConstraint(SpringLayout.NORTH,txtUsername,
            15,
            SpringLayout.NORTH,panel);

        panel.add(txtSenha);
        txtSenha.setOpaque(false);
        txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        layout.putConstraint(SpringLayout.WEST, txtSenha,
            5,
            SpringLayout.EAST, senha);     
        layout.putConstraint(SpringLayout.NORTH, txtSenha,
            45,
            SpringLayout.NORTH,panel);

        panel.add(txtConfirm);
        txtConfirm.setOpaque(false);
        txtConfirm.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        layout.putConstraint(SpringLayout.WEST, txtConfirm,
            5,
            SpringLayout.EAST, confirm);     
        layout.putConstraint(SpringLayout.NORTH, txtConfirm,
            75,
            SpringLayout.NORTH,panel);

        panel.add(buttonCadastrar);
        buttonCadastrar.setForeground(Color.WHITE);
        buttonCadastrar.setOpaque(false);
        buttonCadastrar.setBorderPainted(false);
        buttonCadastrar.setContentAreaFilled(false); 
        layout.putConstraint(SpringLayout.WEST, buttonCadastrar,
            95,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, buttonCadastrar,
            30,
            SpringLayout.NORTH,txtConfirm);

        panel.add(buttonVoltar);
        buttonVoltar.setForeground(Color.WHITE);
        buttonVoltar.setOpaque(false);
        buttonVoltar.setBorderPainted(false);
        buttonVoltar.setContentAreaFilled(false);
        layout.putConstraint(SpringLayout.WEST, buttonVoltar,
            0 ,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonVoltar,
            0,
            SpringLayout.SOUTH,panel);

        panel.add(buttonExit);
        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);
        buttonExit.setForeground(Color.RED);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
            0 ,
            SpringLayout.EAST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonExit,
            0,
            SpringLayout.SOUTH,panel);

        buttonExit.addActionListener(this);
        buttonVoltar.addActionListener(this);
        buttonCadastrar.addActionListener(this);
        clearTextFields();

        return panel;   
    }

    private void clearTextFields()
    {
        Component components[] = panel.getComponents();
        for (Component c: components)
        {
            if (c instanceof JTextField)
                ((JTextField) c).setText("");
        }   
    }

    public boolean verifaCampos()
    {
        txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        txtConfirm.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.WHITE));
        if(txtUsername.getText().trim().isEmpty() == false)
        {    
            if((txtSenha.getText().trim().isEmpty()==false) && (txtConfirm.getText().equals(txtSenha.getText())))
            {
                try
                {
                    controller.existUsers(txtUsername.getText());
                    if(txtSenha.getText().length() > 5 && txtSenha.getText().length() < 11) return true;
                    else
                    {
                        txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                        txtConfirm.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                        txtSenha.setText("");
                        txtConfirm.setText("");
                        JOptionPane.showMessageDialog(frame
                        ,"A senha deve ter de 6 a 10 caracteres.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                        return false;
                    }
                }catch(Exception e)
                {

                    txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                    JOptionPane.showMessageDialog(frame
                    ,"Nome de usu�rio j� cadastrado.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());
                    txtUsername.setText("");

                    return false;
                }
            }
            else
            {
                if(txtSenha.getText().trim().isEmpty())
                {
                    txtSenha.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                    JOptionPane.showMessageDialog(frame
                    ,"Informe a senha.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());

                    return false;
                }
                else
                {
                    txtConfirm.setText("");
                    txtConfirm.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
                    JOptionPane.showMessageDialog(frame
                    ,"A senha n�o foi confirmadada","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());

                    return false; 
                }    
            }

        }
        else
        {
            txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
            JOptionPane.showMessageDialog(frame
            ,"Informe um nome de usu�rio.","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());

            return false;
        }

    }

    public Users getTxt()
    {
        Users getusername = new Users();
        getusername.setUsername(txtUsername.getText());
        getusername.setSenha(txtSenha.getText());
        return getusername;
    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonCadastrar))
        {
            buttonCadastrar_Click();
        }

        if(e.getSource().equals(buttonVoltar))
        {
            buttonVoltar_Click();
        }
        if(e.getSource().equals(buttonExit))
        {               
            int option = JOptionPane.showConfirmDialog(frame,
                    "Deseja sair do programa?",
                    appName,
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {                       
                System.exit(0);
            }
        }
    }

    public void buttonCadastrar_Click()
    {
        if(verifaCampos())
        {
            usernameCadastro = getTxt();
            try
            {
                controller.create(usernameCadastro);
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(frame
                ,"Ocorreu um erro. \nN�o foi poss�vel cadastrar o novo usu�rio","Algo deu errado", 
                JOptionPane.INFORMATION_MESSAGE,fundo.getAtencao());    
            }
            buttonVoltar_Click();
        }
    }

    public void buttonVoltar_Click()
    {
        {        
            PanelLogin panel2 = new PanelLogin(database, appName);   
            JPanel panel3 = panel2.alterarPanel(frame);
            frame.getContentPane().removeAll();
            frame.add(panel3);
            frame.pack();
            frame.setSize(323,210);

        }     
    }
}
