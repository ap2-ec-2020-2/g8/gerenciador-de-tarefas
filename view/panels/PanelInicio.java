package view.panels;

import javax.swing.*;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.Color;
import view.Utils;

public class PanelInicio  implements ActionListener
{
    private Fundos fundo = new Fundos();
    private JFrame frame;
    private JPanel panelInicio;
    private JButton buttonSobre;
    private JButton buttonExit;
    private JButton buttonVoltar;
    Utils util = new Utils();
    String appName;

    public JPanel setPanel(JFrame frame, String n)
    {
        this.frame = frame;
        this.appName = n;
        panelInicio =  new Panel(fundo.getInicio());

        buttonSobre = new JButton("Sobre");
        buttonSobre.setOpaque(false);
        buttonSobre.setBorderPainted(false);
        
        buttonExit = new JButton("Sair");
        
        buttonExit.addActionListener(this);        
        buttonSobre.addActionListener(this);
        
        SpringLayout layout = new SpringLayout();
        panelInicio.setLayout(layout);
        
        panelInicio.add(buttonSobre);
        layout.putConstraint(SpringLayout.WEST, buttonSobre,
                        243 ,
                        SpringLayout.WEST, panelInicio);     
                    layout.putConstraint(SpringLayout.SOUTH, buttonSobre,
                        -70,
                        SpringLayout.SOUTH,panelInicio);
        panelInicio.add(buttonExit);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
                        0 ,
                        SpringLayout.EAST, panelInicio);     
                    layout.putConstraint(SpringLayout.SOUTH, buttonExit,
                        0,
                        SpringLayout.SOUTH,panelInicio);
        buttonSobre.setOpaque(false);
        buttonSobre.setBorderPainted(false);
        buttonSobre.setContentAreaFilled(false);
                        
        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);
        buttonExit.setForeground(Color.RED);
        return panelInicio;
    }
    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonExit))
        {               
           int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {                       
                System.exit(0);
            }
        }

        if(e.getSource().equals(buttonSobre))
        {        
                PanelSobre panelSobre = new PanelSobre();   
                JPanel panel = panelSobre.setPanel(frame, appName);
                frame.getContentPane().removeAll();
                frame.add(panel);
                util.setFrame(frame);
        }   
    }
     
}
