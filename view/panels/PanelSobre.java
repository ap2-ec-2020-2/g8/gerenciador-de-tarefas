package view.panels;

import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.Font;
import view.Utils;
import java.awt.Dimension;
public class PanelSobre extends JPanel implements ActionListener
{
    private Fundos fundo = new Fundos();   
    private JFrame frame;
    private JPanel panel;
    private Utils util = new Utils();
    private JButton buttonExit;
    private JButton buttonVoltar;
    private JLabel titulo;
    private JTextArea txt;
    private SpringLayout layout;
    String appName;
    public void paintComponent(Graphics g)
    {   
        super.paintComponent(g);
        g.drawImage(fundo.getSobre().getImage(), 0, 0, this);
    }

    public JPanel setPanel(JFrame frame, String n)
    {
        this.appName = n;
        this.frame = frame;
        panel =  new Panel(fundo.getSobre());
        
        txt= new JTextArea();
        txt.setLineWrap(true);
        txt.setWrapStyleWord(true);
        txt.setEditable(false);
        
        layout = new SpringLayout();
        panel.setLayout(layout);
        JScrollPane scrollPane = new JScrollPane(txt); 
        scrollPane.setPreferredSize(new Dimension(200, 200));
     
        
        buttonVoltar = new JButton("Voltar");

        buttonVoltar.setOpaque(false);
        buttonVoltar.setContentAreaFilled(false);
        buttonVoltar.setBorderPainted(false);

        buttonExit = new JButton("Sair");
      

        titulo = new JLabel("Um pouco sobre o EasyTask...");

        titulo.setFont(new Font("Dialog", Font.BOLD, 15));
        
        
        txt.setText("          Todos sabemos o qu�o corrida � a vida de quem possui C�lculo,\n"+
        " Geometria, �lgebra em sua grade de mat�rias, e muitas das vezes, ele se perde na"+
        " sua pr�pria bagun�a.\n           Propomos ent�o a utiliza��o de um facilitador. Esse � o"+
        " prop�sito do EasyTask."+"\n           Criado de estudantes para estudantes, o EasyTask�surgiu "+
        "para facilitar a vida dos aspirantes a engenheiros."+ "\n           Mas como?\n           Voc� pode cadastrar"+
        " todas suas atividades de diferentes mat�rias no app, atribuir o status de conclus�o / n�o"+
        " conclus�o, adicionar e alterar as tarefas ja existentes e muito mais!\n           Facilite sua vida,"+
        " use EasyTask.");
        txt.setFont(new Font("SansSerif", Font.PLAIN, 13));
        txt.setOpaque(false);
        txt.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        
        buttonExit.addActionListener(this);        
        buttonVoltar.addActionListener(this);
        
        JViewport viewport = new JViewport();
        viewport.setView(txt);
        viewport.setOpaque(false);
        scrollPane.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, Color.WHITE));
        scrollPane.setOpaque(false);
        scrollPane.setViewport(viewport);
        scrollPane.setWheelScrollingEnabled(true);
        panel.add(scrollPane);
        panel.add(titulo);

        layout.putConstraint(SpringLayout.WEST, titulo,
            170 ,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.NORTH, titulo,
            10,
            SpringLayout.NORTH,panel);

        layout.putConstraint(SpringLayout.NORTH, scrollPane,
            50 ,
            SpringLayout.NORTH, panel);               
        layout.putConstraint(SpringLayout.WEST, scrollPane,
            40 ,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, scrollPane,
            -30,
            SpringLayout.SOUTH,panel);
        layout.putConstraint(SpringLayout.EAST,scrollPane,
            -30,
            SpringLayout.EAST,panel);              

        panel.add(buttonVoltar);
        layout.putConstraint(SpringLayout.WEST, buttonVoltar,
            0 ,
            SpringLayout.WEST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonVoltar,
            0,
            SpringLayout.SOUTH,panel);

        panel.add(buttonExit);
        layout.putConstraint(SpringLayout.EAST, buttonExit,
            0 ,
            SpringLayout.EAST, panel);     
        layout.putConstraint(SpringLayout.SOUTH, buttonExit,
            0,
            SpringLayout.SOUTH,panel);
        buttonExit.setOpaque(false);
        buttonExit.setBorderPainted(false);
        buttonExit.setContentAreaFilled(false);
        buttonExit.setForeground(Color.RED);
        return panel;
    }

    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource().equals(buttonExit))
        {               
            int option;

            option = JOptionPane.showConfirmDialog(frame,
                "Deseja sair do programa?",
                appName,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,fundo.getExit());

            if (option == JOptionPane.YES_OPTION)
            {                       
                System.exit(0);
            }
        }

        if(e.getSource().equals(buttonVoltar))
        {        
            PanelInicio panel2 = new PanelInicio();   
            JPanel panel3 = panel2.setPanel(frame, appName);
            frame.getContentPane().removeAll();
            frame.add(panel3);
            util.setFrame(frame);
        }   

    }
}