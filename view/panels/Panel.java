package view.panels;
import javax.swing.*;
import java.awt.Graphics;

public class Panel extends JPanel{
    ImageIcon fundo;
    public Panel(ImageIcon fundo)
    {       
        this.fundo = fundo;
    }
    public void paintComponent(Graphics g)
    {   
        super.paintComponent(g);
        g.drawImage(fundo.getImage(), 0, 0, this);
    }
}