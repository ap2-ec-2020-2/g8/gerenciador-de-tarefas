import view.FrameLogin;
import model.Database;
import javax.swing.JFrame;

public class App
{
    public static void main (String args [])
    {
        Database db;
        String dbName = "users.db";
        String appName = "EasyTask";        
        
        db = new Database(dbName);
       
        FrameLogin frame = new FrameLogin(db, appName);
        frame.login();
    }
}